/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

import android.util.Log;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule14 extends Rule {

    String x = null;
    //Nếu có "ảu" thì từ này bắt buộc phải nằm trong từ "nhảu"

    public boolean checkValid(String x) {
        this.x = x;
        if (x.contains("ảu")) {
            if (!x.equalsIgnoreCase("nhảu")) {
                return false;
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule14 Error -->" + x);
    }

}
