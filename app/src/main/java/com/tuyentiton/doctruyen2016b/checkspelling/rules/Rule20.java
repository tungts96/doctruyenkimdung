/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule20 extends Rule {

    String x = null;

    //Nguyên âm "ể" không có phụ âm khác đi sau.
    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ể")) {
                String x2 = x.charAt(i + 1) + "";
                if (x2.equalsIgnoreCase("n") || x2.equalsIgnoreCase("m")) {
                    return true;
                }
                if (Constant.CONSONANT.contains(x2)) {
                    return false;
                }
            }
        }
        return true;
    }

    public String show(){
        return "Rule20 Error -->" + x;
    }
}
