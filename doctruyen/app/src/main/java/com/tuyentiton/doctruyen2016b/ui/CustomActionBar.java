package com.tuyentiton.doctruyen2016b.ui;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuyentiton.doctruyen2016b.R;

/**
 * Created by Tuyen Ti Ton on 2/16/2017.
 */

public class CustomActionBar extends RelativeLayout implements View.OnClickListener {

    public static final int SHOW_TITLE = 1;
    public static final int SHOW_SEARCH = 2;
    private NaviListener naviListener;
    //private SearchListener searchListener;
    private boolean isSearch = false;


    public interface NaviListener {
        void onRightClick();
        void onLeftClick();
    }

//    public interface SearchListener {
//        void searchAction(String key);
//        void searchCancel();
//    }

    public CustomActionBar(Context context) {
        super(context);
        initView();
    }
    public CustomActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CustomActionBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    private ImageView imgLeft;
    private SearchView imgRight;
    private TextView tvTitle;

//    public void setShowType(int type) {
//        switch (type) {
//            case SHOW_TITLE: {
//                tvTitle.setVisibility(View.VISIBLE);
//                isSearch = false;
//                break;
//            }
//            case SHOW_SEARCH: {
////                edtSearch.setVisibility(View.VISIBLE);
////                tvTitle.setVisibility(View.GONE);
////                isSearch = true;
//                break;
//            }
//        }
//    }

    private void initView() {
        View navigation = View.inflate(getContext(), R.layout.custom_action_bar, null);
        if (navigation == null) {
            return;
        }
        imgLeft = (ImageView) navigation.findViewById(R.id.imgLeft);
        tvTitle = (TextView) navigation.findViewById(R.id.tvTitle);

        addView(navigation, new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT));

                if (imgLeft != null) {
                    imgLeft.setOnClickListener(this);
                }
//                if (imgRight != null) {
//                    imgRight.setOnQueryTextListener(this);
//                }
//                if (edtSearch != null) {
//                    edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                        @Override
//                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                        if (searchListener != null) {
//                            searchListener.searchAction(edtSearch.getText().toString());
//                        }
//                        return true;
//                    }
//
//                    return false;
//                }
//            });
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgLeft: {
                if (naviListener != null) {
                    naviListener.onLeftClick();
                }
                break;
            }
//            case R.id.imgSearch: {
////                if (naviListener != null) {
////                    naviListener.onRightClick();
////                }
////                break;
//            }
            default:
                break;
        }
    }


//   public void setImgRight(int resource){
//        imgRight.setImageResource(resource);
//    }

    public void setTvTitle(String title) {
        tvTitle.setText(title);
    }

    public void setNaviListener(NaviListener naviListener) {
        this.naviListener = naviListener;
    }

//    public void setSearchListener(SearchListener searchListener) {
//        this.searchListener = searchListener;
//    }

    public boolean isSearch(){
        return isSearch;
    }

}
