/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule1 extends Rule {
    String x =null;
    //Các nguyên âm trong từ phải đứng cạnh nhau, không có phụ âm chen vào giữa
    public boolean checkValid(String x) {
        this.x=x;
        String vowel[] = Constant.DOUBLE_VOWEL;
        String consonant = Constant.CONSONANT;
        for (int j = 0; j < vowel.length; j++) {
            String v = vowel[j];
            for (int i = 0; i < x.length(); i++) {
                if (i < (x.length() - 2)) {
                    if (v.charAt(0) == x.charAt(i)) {
                       if(v.charAt(1) == x.charAt(i + 2)){
                           if(consonant.contains(x.charAt(i+1)+"")){
                               return false;
                           }
                       }
                    }
                }else{
                    break;
                }
            }
        }
        return true;
    }
    
    public String show(){
        return "Rule1 Error -->" + x;
    }
}
