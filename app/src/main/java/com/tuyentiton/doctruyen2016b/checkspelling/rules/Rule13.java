/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule13 extends Rule {

    String x = null;
    // Các nguyên âm được phép sau nguyên âm "ì": ìu, ìa, 
    //Các nguyên âm được phép sau nguyên âm "ỉ": ỉu, mỉa, 
    //Các nguyên âm được phép sau nguyên âm "ị": ịu, ịa, 
    //Các nguyên âm được phép sau nguyên âm "ĩ": ĩu, ĩa, 
    //Các nguyên âm được phép sau nguyên âm "í": íu, ía, 
    String temp = "au";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ì") || x1.equalsIgnoreCase("ỉ") || x1.equalsIgnoreCase("ị") || x1.equalsIgnoreCase("ĩ") || x1.equalsIgnoreCase("í")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!temp.contains(x2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public String show(){
        return "Rule13 Error -->" + x;
    }
}

