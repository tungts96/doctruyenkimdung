package com.tuyentiton.doctruyen2016b.checkspelling;

/**
 * Created by Son tung on 4/27/2017.
 */

public class SearchStringWithRegex {

    private String stringQuery;
    ConvertUnsigned unsigned;

    public SearchStringWithRegex(){
        unsigned = new ConvertUnsigned();
    }

    public void setStringQuery(String stringQuery) {
        stringQuery = unsigned.convertString(stringQuery);
        this.stringQuery ="(.*)" + stringQuery.replace(" ","(.*)")+"(.*)";
    }

    public boolean checkString(String s){
        if (s.matches(stringQuery)){
            return true;
        } else  {
            return false;
        }
    }
}
