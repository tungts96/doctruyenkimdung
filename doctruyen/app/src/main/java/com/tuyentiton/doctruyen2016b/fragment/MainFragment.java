package com.tuyentiton.doctruyen2016b.fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.activities.MainActivity;
import com.tuyentiton.doctruyen2016b.adapter.StoryAdapter;
import com.tuyentiton.doctruyen2016b.checkspelling.ConvertUnsigned;
import com.tuyentiton.doctruyen2016b.database.KimDungSQLite;
import com.tuyentiton.doctruyen2016b.model.Story;

import java.util.ArrayList;

import static com.tuyentiton.doctruyen2016b.database.KimDungSQLite.DATABASE_TABLE_KIMDUNG;

/**
 * Created by Son tung on 2/28/2017.
 */

public class MainFragment extends android.support.v4.app.Fragment implements View.OnClickListener {


    private RecyclerView rcvListStory, rcvListLoveStory;
    private static ArrayList<Story> arrStories, arrLoveStories;
    ArrayList<Story> coppyListStory,coppyListLoveStory;
    private static StoryAdapter storyAdapter, loveStoryAdapter;
    public static String ADAPTER_LIST_STORY = "ListStory";
    public static String ADAPTER_LIST_LOVE_STORY = "ListLoveStory";

    private LinearLayout linearLayoutMain;
    private RelativeLayout changeScreen;
    private ImageView imgScreen,imgChangeScreen;
    private boolean flagChangeScreen = true;
    private SearchView searchView ;

    private ConvertUnsigned convertUnsigned;

    private int position;
    View root;
    private SQLiteDatabase database;
    private Context context;

    public MainFragment() {}

    public MainFragment(Context context) {
        this.context = context;
    }

    public MainFragment(int position) {
        this.position = position;
    }

    public static MainFragment newInstance(int position) {
        MainFragment fragment = new MainFragment(position);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_main, container, false);
        Log.e("ST","creatMain");
        addControls();
        try {
            switch (position) {
                case 0:
                    initFramentListStory();
                    Log.e("ST","creat");
                    break;
                case 1:
                    initFramentListLoveStory();
                    break;
//                case 2:
//                    initFramentIntroAboutStory();
//                    break;
                default:
                    initFramentListStory();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        initSearchView();
        return root;
    }

    private void initSearchView() {
        coppyListStory = new ArrayList<>();
        coppyListStory = getListStories();
        coppyListLoveStory = new ArrayList<>();
        coppyListLoveStory = getListLoveStories();
        searchView = (SearchView) getActivity().findViewById(R.id.imgSearch);
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filter(convertUnsigned.convertString(query));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(convertUnsigned.convertString(newText));
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                imgScreen.setVisibility(View.VISIBLE);
                return false;
            }
        });
    }

    private void addControls() {
        convertUnsigned = new ConvertUnsigned(); //Convert Tieng viet có dau thành không dấu
        rcvListStory = (RecyclerView) root.findViewById(R.id.rcvListStory);
        rcvListLoveStory = (RecyclerView) root.findViewById(R.id.rcvListLoveStory);
        linearLayoutMain = (LinearLayout) root.findViewById(R.id.linearLayoutMain);
        arrStories = new ArrayList<>();arrStories = getListStories();
        arrLoveStories = new ArrayList<>();arrLoveStories = getListLoveStories();
        changeScreen = (RelativeLayout) root.findViewById(R.id.changeScreen);
        imgScreen = (ImageView) root.findViewById(R.id.imgScreen);
        imgScreen.setOnClickListener(this);
    }

//    private void initFramentIntroAboutStory() {
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.setMargins(20, 2, 10, 10);
//
//        TextView tvIntroApp = new TextView(getContext());
//        tvIntroApp.setLayoutParams(params);
//        tvIntroApp.setText("Môn học : Lập trình Android \n" +
//                "Mã học phần : IT3620 \n" +
//                "Product by : Sơn Tùng & Tuyển Tí Tởn \n" +
//                "Sản phẩm nằm trong phạm vi môn học nên còn nhiều sai sót");
//        tvIntroApp.setTextColor(getResources().getColor(R.color.black));
//        tvIntroApp.setTextSize(15);
//        Button btnFeedBack = new Button(getContext());
//        params.gravity = Gravity.CENTER_HORIZONTAL;
//        btnFeedBack.setLayoutParams(params);
//        btnFeedBack.setText("Góp ý");
//        linearLayoutMain.addView(tvIntroApp);
//        linearLayoutMain.addView(btnFeedBack);
//    }

    private void initFramentListLoveStory() {
        //arrLoveStories = getListLoveStories();
        loveStoryAdapter = new StoryAdapter(getContext(), arrLoveStories, ADAPTER_LIST_LOVE_STORY, 0);
        rcvListLoveStory.setAdapter(loveStoryAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        rcvListLoveStory.setLayoutManager(gridLayoutManager);
    }

    private void initFramentListStory() {
        GridLayoutManager gridLayoutManager;
        changeScreen.setVisibility(View.VISIBLE);
       // arrStories = getListStories();
        if (flagChangeScreen){
            storyAdapter = new StoryAdapter(getActivity(), arrStories, ADAPTER_LIST_STORY, 1);
            gridLayoutManager = new GridLayoutManager(getContext(), 1);
        } else {
            storyAdapter = new StoryAdapter(getActivity(), arrStories, ADAPTER_LIST_STORY, 0);
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }
        rcvListStory.setAdapter(storyAdapter);
        rcvListStory.setLayoutManager(gridLayoutManager);
        rcvListStory.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("STTT",dx+" "+dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("STTTState",newState+"");
            }
        });
    }

    public ArrayList<Story> getListStories() {
        ArrayList<Story> listStory = new ArrayList<>();
        listStory.clear();
        database = KimDungSQLite.initDatabase(getContext());
        Cursor c = database.rawQuery("Select * from " + DATABASE_TABLE_KIMDUNG, null);
        while (c.moveToNext()) {
            Story story = new Story(c.getInt(0), c.getString(1));
            story.setImage(c.getString(3));
            story.setDescribe(c.getString(4));
            if (c.getInt(2) == 0) {
                story.setLike(false);
            } else {
                story.setLike(true);
            }
            listStory.add(story);
        }
        c.close();
        return listStory;
    }

    public ArrayList<Story> getListLoveStories() {
        ArrayList<Story> listLoveStory = new ArrayList<>();
        listLoveStory.clear();
        database = KimDungSQLite.initDatabase(getContext());
        Cursor c = database.rawQuery("Select * from " + DATABASE_TABLE_KIMDUNG + " Where auID=1", null);
        while (c.moveToNext()) {
            Story story = new Story(c.getInt(0), c.getString(1));
            story.setImage(c.getString(3));
            story.setDescribe(c.getString(4));
            story.setLike(true);
            listLoveStory.add(story);
        }
        c.close();
        return listLoveStory;
    }

    public void update(Story story, String type) {
        if (type.equals(ADAPTER_LIST_STORY)) {
            if (story.isLike()) {
                arrLoveStories.add(story);
                loveStoryAdapter.notifyDataSetChanged();
            } else {
                for (int i = 0; i < arrLoveStories.size(); i++) {
                    if (story.getIdStory() == arrLoveStories.get(i).getIdStory()) {
                        arrLoveStories.remove(i);
                        loveStoryAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
            MainActivity.updateView();
        } else if (type.equals(ADAPTER_LIST_LOVE_STORY)) {
            loveStoryAdapter.notifyDataSetChanged();
            MainActivity.updateView();
        }
    }

    public void filter(String text) {
        switch (MainActivity.TAB_SELECTED){
            case 0:
                Log.e("ST1", arrStories.size() + "");
                ArrayList<Story> arr = new ArrayList<>();
                if (text.isEmpty()) {
                    arr.addAll(coppyListStory);
                } else {
                   // text = text.toLowerCase();
                    for (Story item : coppyListStory) {
                        String nameStory = item.getName();
                        if (convertUnsigned.convertString(nameStory).contains(text)) {
                            arr.add(item);
                        }
                    }
                }
                Log.e("ST2", arrStories.size() + "");
                arrStories.clear();
                arrStories.addAll(arr);
                storyAdapter.updateData(arr);
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.imgScreen) {
            Animation animFade = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
            imgScreen.startAnimation(animFade);
            changeScreen();
        }
    }

    private void changeScreen() {
        if (flagChangeScreen) {
            storyAdapter = new StoryAdapter(getContext(), arrStories, ADAPTER_LIST_STORY, 0);
            rcvListStory.setAdapter(storyAdapter);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
            rcvListStory.setLayoutManager(gridLayoutManager);
            imgScreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_linear));
            flagChangeScreen = false;
        } else {
            storyAdapter = new StoryAdapter(getContext(), arrStories, ADAPTER_LIST_STORY, 1);
            rcvListStory.setAdapter(storyAdapter);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
            rcvListStory.setLayoutManager(gridLayoutManager);
            flagChangeScreen = true;
            imgScreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_grid));
        }
    }
}
