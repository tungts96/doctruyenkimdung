package com.tuyentiton.doctruyen2016b.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.interfaces.RecyclerviewItemClick;
import com.tuyentiton.doctruyen2016b.model.Chapter;

import java.util.List;

/**
 * Created by Son tung on 3/14/2017.
 */

public class ChapterAdater extends RecyclerView.Adapter<ChapterAdater.ChapterViewHolder>{

    private RecyclerviewItemClick recyclerviewItemClick;
    private List<Chapter> listChapter;
    private Context context;
    private LayoutInflater mLayoutInflater;
    private Uri res;
    private final int VIEW_TYPE_ITEM=0;
    private final int VIEW_TYPE_LOADING=1;

    private int viewType;

    public ChapterAdater(Context context, List<Chapter> listChapter) {
        this.context = context;
        this.listChapter = listChapter;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ChapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // if (viewType == VIEW_TYPE_ITEM){
            this.viewType = VIEW_TYPE_ITEM;
            View itemChapter = mLayoutInflater.inflate(R.layout.item_chapter,null);
            return new ChapterViewHolder(itemChapter,VIEW_TYPE_ITEM);
//        } else {
//            this.viewType = VIEW_TYPE_LOADING;
//            View item = mLayoutInflater.inflate(R.layout.item_loading_chapter,null);
//            return new ChapterViewHolder(item,VIEW_TYPE_LOADING);
//        }
    }

    @Override
    public void onBindViewHolder(ChapterViewHolder holder, int position) {
     //  if (viewType == VIEW_TYPE_LOADING){
     //       holder.progressBar.setIndeterminate(true);
      //  } else if (viewType == VIEW_TYPE_ITEM){
            Chapter chapter = listChapter.get(position);
            holder.tvTitelChapter.setText(chapter.getNameChapter());
            if (!chapter.getTimeRead().isEmpty()){
                holder.tvTimeRead.setText("Thời gian đọc trước: "+chapter.getTimeRead());
            }
            setImageChapter(holder.imgChapter);
      //  }
    }

    @Override
    public int getItemViewType(int position) {
        return listChapter.get(position) == null?VIEW_TYPE_LOADING:VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listChapter.size();
    }

    public void setImageChapter(CircularImageView img){
        Picasso.with(context).load(res).fit().centerInside().into(img);
    }

    public void setRes(Uri res){
        this.res = res;
    }

    class ChapterViewHolder extends RecyclerView.ViewHolder  {
        private TextView tvTitelChapter,tvTimeRead;
        private CircularImageView imgChapter;
        private ProgressBar progressBar;

        public ChapterViewHolder(View itemView,int viewType) {
            super(itemView);
           // if (viewType == VIEW_TYPE_ITEM){
                tvTitelChapter = (TextView) itemView.findViewById(R.id.tvTitleChapter);
                tvTimeRead = (TextView) itemView.findViewById(R.id.tvTimeRead);
                imgChapter = (CircularImageView) itemView.findViewById(R.id.imgChapter);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (recyclerviewItemClick != null) {
                            recyclerviewItemClick.itemClick(getAdapterPosition());
                        }
                    }
                });
//            } else if (viewType == VIEW_TYPE_LOADING){
//                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBarLoadChapter);
//            }
        }
    }

    public void setRecyclerviewItemClick(RecyclerviewItemClick recyclerviewItemClick){
        this.recyclerviewItemClick = recyclerviewItemClick;
    }
}
