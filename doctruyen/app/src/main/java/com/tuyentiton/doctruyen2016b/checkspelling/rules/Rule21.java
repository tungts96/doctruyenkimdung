/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule21 extends Rule {

    String x = null;
    //Nguyên âm sau "o" để tạo thành cặp nguyên âm: "oa", "oi", "oe",
    String temp = "aieo";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("o")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!temp.contains(x2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule21 Error -->" + x);
    }
}
