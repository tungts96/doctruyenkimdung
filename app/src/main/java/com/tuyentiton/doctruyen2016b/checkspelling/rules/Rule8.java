/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule8 extends Rule {

    //Có những phụ âm đứng cuối nhưng trước nó không được phép chứa phụ âm khác: n m t p c 
    String x = null;
    String consonant = Constant.CONSONANT;
    String temp = "nmtpc";

    public boolean checkValid(String x) {
        this.x = x;
        String last = x.charAt(x.length() - 1) + "";
        if (temp.contains(last)) {
            String before = x.charAt(x.length() - 2) + "";
            if (consonant.contains(before)) {
                return false;
            }
        }
        return true;
    }

    public String show(){
        return "Rule8 Error -->" + x;
    }

}
