package com.tuyentiton.doctruyen2016b.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.fragment.ChapterFragment;
import com.tuyentiton.doctruyen2016b.model.Story;

public class ReadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        replaceFragmentContent(new ChapterFragment());
    }

    public Story getStory() {
        Intent intent = getIntent();
        Story story = (Story) intent.getSerializableExtra("STORY");
        return story;
    }

    public void replaceFragmentContent(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fmgr = getSupportFragmentManager();
            FragmentTransaction ft = fmgr.beginTransaction();
            ft.replace(R.id.container_body, fragment);
            ft.commit();
        }
    }

    public void addFragmentContent(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fmgr = getSupportFragmentManager();
            FragmentTransaction ft = fmgr.beginTransaction();
            ft.add(R.id.container_body, fragment);
            ft.addToBackStack(fragment.getClass().getSimpleName());
            ft.commit();
        }
    }

}

