/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule25 extends Rule{
    
    String x = null;

    //Nguyên âm "ơ", "ớ", "ờ", "ở", "ợ" thì chỉ có đằng sau là nguyên âm "i"
    //Nguyên âm "ô", "ố", "ồ", "ổ", "ộ", "ỗ" thì chỉ có đằng sau là nguyên âm "i"
    public boolean checkValid(String x) {
        this.x = x; 
        if(x.equalsIgnoreCase("hươu") || x.equalsIgnoreCase("rượu") || x.equalsIgnoreCase("bướu")){
            return true;
        }
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ơ") || x1.equalsIgnoreCase("ớ") || x1.equalsIgnoreCase("ờ") || x1.equalsIgnoreCase("ở")
                    || x1.equalsIgnoreCase("ợ") || x1.equalsIgnoreCase("ô") || x1.equalsIgnoreCase("ố")
                    || x1.equalsIgnoreCase("ồ") || x1.equalsIgnoreCase("ổ") || x1.equalsIgnoreCase("ộ") || x1.equalsIgnoreCase("ỗ")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!x2.equalsIgnoreCase("i")) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    public void show() {
        Log.e("checkSpell","Rule25 Error -->" + x);
    }
}
