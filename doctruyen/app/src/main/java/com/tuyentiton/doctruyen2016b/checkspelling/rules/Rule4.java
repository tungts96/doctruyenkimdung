/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule4 extends Rule {
    String x = null;

    //Nếu một từ có 3 phụ âm cạnh nhau => đó phải là ngh
    public boolean checkValid(String x) {
        this.x = x;
        String consonant = Constant.CONSONANT;
        for (int i = 0; i < x.length(); i++) {
            if (i < (x.length() - 2)) {
                String x1 = x.charAt(i) + "";
                String x2 = x.charAt(i + 1) + "";
                String x3 = x.charAt(i + 2) + "";
                if (consonant.contains(x1)) {
                    if (consonant.contains(x2)) {
                        if (consonant.contains(x3)) {
                            String result = x1 + x2 + x3;
                            if (!result.equalsIgnoreCase("ngh")) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule4 Error -->" + x);
    }

}
