package com.tuyentiton.doctruyen2016b.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.interfaces.RecyclerviewItemClick;
import com.tuyentiton.doctruyen2016b.model.Chapter;

import java.util.List;

/**
 * Created by Son tung on 3/14/2017.
 */

public class ChapterAdater extends RecyclerView.Adapter<ChapterAdater.ChapterViewHolder> {

    private RecyclerviewItemClick recyclerviewItemClick;
    private List<Chapter> listChapter;
    private Context context;
    private LayoutInflater mLayoutInflater;
    private Uri res;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private int viewType;

    public ChapterAdater(Context context, List<Chapter> listChapter) {
        this.context = context;
        this.listChapter = listChapter;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ChapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.viewType = VIEW_TYPE_ITEM;
        View itemChapter = mLayoutInflater.inflate(R.layout.item_chapter, null);
        return new ChapterViewHolder(itemChapter, VIEW_TYPE_ITEM);
    }

    @Override
    public void onBindViewHolder(ChapterViewHolder holder, int position) {
        Chapter chapter = listChapter.get(position);
        holder.tvTitelChapter.setText(chapter.getNameChapter());
        holder.tvPosition.setText(String.valueOf(position + 1));
        if (chapter.getResult() == null) {
            holder.tvTimeRead.setVisibility(View.VISIBLE);
            holder.tvResult.setVisibility(View.GONE);
            if (!chapter.getTimeRead().trim().isEmpty()) {
                holder.tvTimeRead.setText("Thời gian đọc trước: " + chapter.getTimeRead());
            }
        } else {
            holder.tvTimeRead.setVisibility(View.GONE);
            holder.tvResult.setVisibility(View.VISIBLE);
            holder.tvResult.setText(chapter.getResult());
        }
        setImageChapter(holder.imgChapter);

    }

    @Override
    public int getItemViewType(int position) {
        return listChapter.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listChapter.size();
    }

    public void setImageChapter(CircularImageView img) {
        Picasso.with(context).load(res).fit().centerInside().into(img);
    }

    public void setRes(Uri res) {
        this.res = res;
    }

    class ChapterViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitelChapter, tvTimeRead, tvPosition, tvResult;
        private CircularImageView imgChapter;

        public ChapterViewHolder(View itemView, int viewType) {
            super(itemView);
            tvTitelChapter = (TextView) itemView.findViewById(R.id.tvTitleChapter);
            tvTimeRead = (TextView) itemView.findViewById(R.id.tvTimeRead);
            imgChapter = (CircularImageView) itemView.findViewById(R.id.imgChapter);
            tvPosition = (TextView) itemView.findViewById(R.id.tvPosition);
            tvResult = (TextView) itemView.findViewById(R.id.tvResult);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerviewItemClick != null) {
                        recyclerviewItemClick.itemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    public void setRecyclerviewItemClick(RecyclerviewItemClick recyclerviewItemClick) {
        this.recyclerviewItemClick = recyclerviewItemClick;
    }
}
