/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule24 extends Rule {

    String x = null;
    //Ta có thể tổng quát hóa lên rằng các từ "ó", "ò", "ỏ", "ọ" có các nguyên âm "e", "i", "a" đằng sau được.

    String temp = "eia";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ó") || x1.equalsIgnoreCase("ò") || x1.equalsIgnoreCase("ỏ") || x1.equalsIgnoreCase("ọ")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!temp.contains(x2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule24 Error -->" + x);
    }
}
