package com.tuyentiton.doctruyen2016b.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.activities.ReadActivity;
import com.tuyentiton.doctruyen2016b.checkspelling.ConvertUnsigned;
import com.tuyentiton.doctruyen2016b.checkspelling.constant.TestRule;
import com.tuyentiton.doctruyen2016b.model.Chapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Son tung on 3/16/2017.
 */

public class ReadChapterFragment extends Fragment implements View.OnClickListener {

    private View root;
    private ImageView imgMoreRead, imgBackRead, imgShare;
    private TextView tvTitelNameStory, tvContentChapter, tvNameChapter;
    private Chapter chapter;
    private ArrayList<Chapter> listChapter;
    private ScrollView scrollView;
    private final int SPEECH_RECOGNITION_CODE = 1;
    private String nameStory;
    private boolean scroll = false;

    private RelativeLayout rlIncreaseTextSize, rlDecreaseTextSize, rlSearch, rlPreviousChapter, rlNextChapter, rlMenu;
    private ImageView imgMenuslideUp;
    private SlidingUpPanelLayout slidingUpPanelLayout;

    //Cỡ Chữ
    private float TEXT_SIZE_OF_NAME_CHAPTER = 25;
    private final String KEY_NAME_CHAPTER = "Name Chapter";
    private float TEXT_SIZE_OF_CONTENT_CHAPTER = 14;
    private final String KEY_CONTENT_CHAPTER = "Content Chapter";

    //Font Chữ
    private final String KEY_TEXT_FONT = "TextFont";
    private Typeface typeface;

    //Convert Chữ có dấu thành k dấu
    ConvertUnsigned convertUnsigned;
    TestRule testRule;

    //Kiem tra chính tả
    private final int CHECK_SPELL_TYPE = 1;
    private final int SEARCH_TYPE = 0;
    ProgressDialog progressDialog;

    //autoScroll
    private boolean autoScroll = false;
    Handler timerHandler;
    TimeRunable timeRunable;


    private boolean hasScroll = false;
    private int line;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_chapter_read, container, false);
        progressDialog = new ProgressDialog(getContext());
        addConTrols();
        initSlidingUpPanel();
        addEvents();
        return root;
    }

    private void initSlidingUpPanel() {
        rlMenu = (RelativeLayout) root.findViewById(R.id.rlmenu);
        imgMenuslideUp = (ImageView) root.findViewById(R.id.imgMenuslideUp);
        rlIncreaseTextSize = (RelativeLayout) root.findViewById(R.id.rlIncreaseTextSize);
        rlIncreaseTextSize.setOnClickListener(this);
        rlDecreaseTextSize = (RelativeLayout) root.findViewById(R.id.rlDecreaseTextSize);
        rlDecreaseTextSize.setOnClickListener(this);
        rlSearch = (RelativeLayout) root.findViewById(R.id.rlSearch);
        rlSearch.setOnClickListener(this);
        rlPreviousChapter = (RelativeLayout) root.findViewById(R.id.rlPreviousChapter);
        rlPreviousChapter.setOnClickListener(this);
        rlNextChapter = (RelativeLayout) root.findViewById(R.id.rlNextChapter);
        rlNextChapter.setOnClickListener(this);
        slidingUpPanelLayout = (SlidingUpPanelLayout) root.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.e("STTTT", "start");
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (previousState == SlidingUpPanelLayout.PanelState.COLLAPSED && newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    imgMenuslideUp.setImageDrawable(getResources().getDrawable(R.drawable.ic_hidemenu));
                } else if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING && newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    imgMenuslideUp.setImageDrawable(getResources().getDrawable(R.drawable.ic_menuslideup));
                }
                Log.e("STTTT", previousState.toString());
                Log.e("STTTT", newState.toString());
            }
        });
    }

    private void addConTrols() {
        convertUnsigned = new ConvertUnsigned();
        testRule = new TestRule();
        imgBackRead = (ImageView) root.findViewById(R.id.imgBack2);
        imgMoreRead = (ImageView) root.findViewById(R.id.imgMoreRead);
        imgShare = (ImageView) root.findViewById(R.id.imgShare);
        scrollView = (ScrollView) root.findViewById(R.id.scrollView);
        tvTitelNameStory = (TextView) root.findViewById(R.id.tvTitleNameStory);
        tvNameChapter = (TextView) root.findViewById(R.id.tvChapterName);
        tvContentChapter = (TextView) root.findViewById(R.id.tvContentChapter);

        if (getArguments() != null) {
            nameStory = getArguments().getString("NAMESTORY");
            chapter = (Chapter) getArguments().getSerializable("CHAPTER");
            hasScroll = getArguments().getBoolean("SEARCH_RESULT");
            listChapter = (ArrayList<Chapter>) getArguments().getSerializable("ARRCHAPTER");
        }

        TEXT_SIZE_OF_NAME_CHAPTER = getTextSizeFromPreferences(KEY_NAME_CHAPTER);
        TEXT_SIZE_OF_CONTENT_CHAPTER = getTextSizeFromPreferences(KEY_CONTENT_CHAPTER);
        typeface = getTextFontFromPreferences(KEY_TEXT_FONT);
        if (typeface != null) {
            Log.e("ST", typeface.getClass().getSimpleName());
            tvNameChapter.setTypeface(typeface);
            tvContentChapter.setTypeface(typeface);
            tvTitelNameStory.setTypeface(typeface);
        }
        if (TEXT_SIZE_OF_NAME_CHAPTER != 0 && TEXT_SIZE_OF_CONTENT_CHAPTER != 0) {
            tvContentChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_CONTENT_CHAPTER);
            tvNameChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_NAME_CHAPTER);
        } else {
            TEXT_SIZE_OF_CONTENT_CHAPTER = 14;
            TEXT_SIZE_OF_NAME_CHAPTER = 25;
            tvContentChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_CONTENT_CHAPTER);
            tvNameChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_NAME_CHAPTER);
        }
        String nameChapter = getNameChapter(chapter);
        tvTitelNameStory.setText(getNumberChapter(chapter));
        tvNameChapter.setText(nameChapter);
        String content = chapter.getContentOfChapter() + " Nguyễ Tuyể yin yun êch xuânh huỳnh";
        chapter.setContentOfChapter(content);
        tvContentChapter.setText(chapter.getContentOfChapter());
        timerHandler = new Handler();
        timeRunable = new TimeRunable(timerHandler);
        if (hasScroll) {
            progressDialog.setMessage("Vui lòng chờ...");
            progressDialog.show();
            tvContentChapter.post(new Runnable() {
                @Override
                public void run() {
                    String result = chapter.getResult();
                    scrollToPosition(result);
                    highlightText(getArrayIndexOfQueryText(convertUnsigned.convertString(result)), result);
                }
            });

        }

    }


    private void scrollToPosition(String key) {
        final int index = chapter.getContentOfChapter().indexOf(key);
        Log.d("__tindexOf", index + "");
        if (tvContentChapter.getLayout() != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            line = tvContentChapter.getLayout().getLineForOffset(index);
        }
        final int y = (int) ((line + 0.5) * tvContentChapter.getLineHeight());
        scrollView.smoothScrollTo(0, y - scrollView.getHeight() / 2);
    }


    private void addEvents() {
        imgMoreRead.setOnClickListener(this);
        imgBackRead.setOnClickListener(this);
        imgMoreRead.setOnClickListener(this);
        imgShare.setOnClickListener(this);
    }

    private String getNameChapter(Chapter chapter) {
        String string = chapter.getNameChapter();
        String[] parts = string.split(":");
        parts[1] = parts[1].substring(1, parts[1].length());
        return parts[1];
    }

    private String getNumberChapter(Chapter chapter) {
        String string = chapter.getNameChapter();
        String[] parts = string.split(":");
        return parts[0];
    }

    //Menu more
    private void showMoreMenu() {
        final int position = getPositionOfChapter(chapter);
        final PopupMenu menu = new PopupMenu(getContext(), imgMoreRead);
        menu.inflate(R.menu.menu_moreread);
        if (autoScroll) {
            MenuItem item1 = menu.getMenu().findItem(R.id.mnuAutoScroll);
            item1.setTitle("Dừng tự động cuộn");
        }
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.mnuTextFont:
                        try {
                            final String[] arrFont = getListTextFontFromAsset();
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Chọn Fonts Chữ");
                            builder.setSingleChoiceItems(arrFont, -1, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveTextFontInPreferences(KEY_TEXT_FONT, arrFont[which]);
                                }
                            });
                            builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.mnuCheckSpelling:
                        TextClass checkSpellClass = new TextClass(getContext(), CHECK_SPELL_TYPE);
                        checkSpellClass.execute(chapter.getContentOfChapter());
                        break;
                    case R.id.mnuAutoScroll:
                        tvContentChapter.setMovementMethod(new ScrollingMovementMethod());
//                        final Handler timerHandler = new Handler();
//                        final Runnable timerRunnable = new Runnable() {
//                            @Override
//                            public void run() {
//                                scrollView.smoothScrollBy(0,5);         // 5 is how many pixels you want it to scroll vertically by
//                                timerHandler.postDelayed(this, 60);     // 10 is how many milliseconds you want this thread to run
//                            }
//                        };
//                        timerHandler.postDelayed(timerRunnable, 0);
                        //
                        if (!autoScroll) {
                            timeRunable.run();
                            autoScroll = true;
                        } else {
                            timerHandler.removeCallbacks(timeRunable);
                            autoScroll = false;
                        }
                        break;
                }
                return false;
            }

        });
        menu.show();
    }

    //Dialog kiểm tra chính tả
    private void showDialogCheckSpell(final ArrayList<String> arrErrorText) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setCancelable(false);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customer_dialog_checkspell);
        dialog.show();
        TextView tvThanks = (TextView) dialog.findViewById(R.id.tvThanks);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnFeedBack);
        ListView lvCheckSpell = (ListView) dialog.findViewById(R.id.lvCheckSpell);
        if (arrErrorText.size() > 0) {
            btnCancel.setText("Gửi lỗi này");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arrErrorText);
            lvCheckSpell.setAdapter(adapter);
        } else {
            tvThanks.setVisibility(View.VISIBLE);
            btnCancel.setText("Cancel");
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            StringBuilder textError = new StringBuilder("Truyện: " + nameStory + "\n" + chapter.getNameChapter() + "\n" + "Các từ bị lỗi: ");

            @Override
            public void onClick(View v) {
                if (!arrErrorText.isEmpty()) {
                    for (String s : arrErrorText) {
                        textError.append(s + ",");
                    }
                    String error = textError.toString().substring(0, textError.length() - 1);
                    dialog.dismiss();
                    sendEmail(error);
                }
                dialog.dismiss();
            }
        });
    }

    //gửi lỗi chính tả
    private void sendEmail(String errorText) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"tungbxars@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Lỗi chính tả");
        intent.putExtra(Intent.EXTRA_TEXT, errorText);
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    //Dialog Tìm kiếm
    private void showDialogSearch() {
        final Dialog dialog = new Dialog(getContext());
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customer_dialog_search);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
        dialog.show();

        final EditText edtQueryText = (EditText) dialog.findViewById(R.id.edtQueryText);
        TextView btnSearchText = (TextView) dialog.findViewById(R.id.btnSearch);
        ImageView btnSearchVoice = (ImageView) dialog.findViewById(R.id.imgMicrophone);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
        btnSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String queryText = edtQueryText.getText().toString();
                if (queryText.length() > 0) {
                    dialog.dismiss();
                    new TextClass(getContext(), SEARCH_TYPE).execute(queryText);
//                    searchInContentChapter(queryText);
                }
            }
        });
        btnSearchVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchVoice();
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //Show dialog text size
    private void dialogTextSize() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.menu_text_sixe);
        dialog.show();
        RelativeLayout increaseTextSize = (RelativeLayout) dialog.findViewById(R.id.increaseTextSize);
        increaseTextSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TEXT_SIZE_OF_CONTENT_CHAPTER < 19) TEXT_SIZE_OF_CONTENT_CHAPTER += 1;
                if (TEXT_SIZE_OF_NAME_CHAPTER < 30) TEXT_SIZE_OF_NAME_CHAPTER += 1;
                tvContentChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_CONTENT_CHAPTER);
                saveTextSizeInPreferences(KEY_CONTENT_CHAPTER, TEXT_SIZE_OF_CONTENT_CHAPTER);
                tvNameChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_NAME_CHAPTER);
                saveTextSizeInPreferences(KEY_NAME_CHAPTER, TEXT_SIZE_OF_NAME_CHAPTER);
            }
        });
        RelativeLayout decreaseTextSize = (RelativeLayout) dialog.findViewById(R.id.decreaseTextSize);
        decreaseTextSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TEXT_SIZE_OF_CONTENT_CHAPTER > 12) TEXT_SIZE_OF_CONTENT_CHAPTER -= 1;
                if (TEXT_SIZE_OF_NAME_CHAPTER > 23) TEXT_SIZE_OF_NAME_CHAPTER -= 1;
                tvContentChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_CONTENT_CHAPTER);
                tvNameChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_NAME_CHAPTER);
                saveTextSizeInPreferences(KEY_CONTENT_CHAPTER, TEXT_SIZE_OF_CONTENT_CHAPTER);
                saveTextSizeInPreferences(KEY_NAME_CHAPTER, TEXT_SIZE_OF_NAME_CHAPTER);
            }
        });
        RelativeLayout cancel = (RelativeLayout) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //lấy cỡ chữ từ trước
    private float getTextSizeFromPreferences(String key) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        float size = sharedPreferences.getFloat(key, 0);
        return size;
    }

    //Luư cỡ chữ
    private void saveTextSizeInPreferences(String key, float textSize) {
        SharedPreferences preferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, textSize);
        editor.commit();
    }

    private String[] getListTextFontFromAsset() throws IOException {
        AssetManager assetManager = getActivity().getAssets();
        String[] arrFontName = assetManager.list("font");
        return arrFontName;
    }

    //Lưu font chữ
    private void saveTextFontInPreferences(String key, String textFont) {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "font/" + textFont);
        tvNameChapter.setTypeface(typeface);
        tvContentChapter.setTypeface(typeface);
        tvTitelNameStory.setTypeface(typeface);
        SharedPreferences preferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, "font/" + textFont);
        editor.commit();
    }

    //Lấy font chữ đc lưu
    private Typeface getTextFontFromPreferences(String key) {
        Typeface tf = null;
        AssetManager assetManager = getActivity().getAssets();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        String font = sharedPreferences.getString(key, "");
        if (font.length() > 0) {
            tf = Typeface.createFromAsset(assetManager, font);
        }
        return tf;
    }

    //get positionChapter of listchapter
    private int getPositionOfChapter(Chapter chapter) {
        int position = -1;
        for (int i = 0; i < listChapter.size(); i++) {
            if (chapter.getIdChapter() == listChapter.get(i).getIdChapter()) {
                position = i;
            }
        }
        return position;
    }

    //bôi đen chữ cần tìm
    private void highlightText(ArrayList<Integer> arrIndexOfKeyWord, String query) {
        SpannableString spannableString = new SpannableString(tvContentChapter.getText());
        BackgroundColorSpan[] backgroundColorSpan =
                spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);
        for (BackgroundColorSpan bgSpan : backgroundColorSpan) {
            spannableString.removeSpan(bgSpan);
        }
        for (int i = 0; i < arrIndexOfKeyWord.size(); i++) {
            spannableString.setSpan(new BackgroundColorSpan(Color.GREEN), arrIndexOfKeyWord.get(i),
                    arrIndexOfKeyWord.get(i) + query.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        tvContentChapter.setText(spannableString);
    }

    private void removeHighlight() {
        SpannableString spannableString = new SpannableString(tvContentChapter.getText().toString());
        BackgroundColorSpan[] backgroundColorSpan =
                spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);
        if (backgroundColorSpan.length > 0) {
            for (BackgroundColorSpan bgSpan : backgroundColorSpan) {
                spannableString.removeSpan(bgSpan);
            }
        }
        tvContentChapter.setText(spannableString);
    }

    //Kiem tra chinh tả
    private ArrayList<String> checkSpelling(String s) {
        return testRule.checkSpelling(s);
    }

    //Lấy in dex tất cả các từ cần tìm
    private ArrayList<Integer> getArrayIndexOfQueryText(String text) {
        ArrayList<Integer> arrIndexOfKeyWord = new ArrayList<>();
        String contentChapter = convertUnsigned.convertString(tvContentChapter.getText().toString());
        Log.e("ST Query2", text);
        int indexOfKeyWord = contentChapter.indexOf(text);
        Log.e("ST query3", indexOfKeyWord + "");
        while (indexOfKeyWord != -1) {
            arrIndexOfKeyWord.add(indexOfKeyWord);
            indexOfKeyWord = contentChapter.indexOf(text, indexOfKeyWord + text.length());
        }
        return arrIndexOfKeyWord;
    }

//    //Search text
//    private void searchInContentChapter(String query){
//        ArrayList<Integer> arrIndexOfKeyWord = new ArrayList<>();
//        Log.e("ST Query1",query + "  "+  convertUnsigned.convertString(query));
//        arrIndexOfKeyWord = getArrayIndexOfQueryText(convertUnsigned.convertString(query));
//        if (arrIndexOfKeyWord.isEmpty()){
//            Toast.makeText(getContext(), "Không có kết quả thỏa mãn", Toast.LENGTH_SHORT).show();
//        } else {
//            highlightText(arrIndexOfKeyWord,query);
//        }
//    }

    //Search voice
    private void searchVoice() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    "Speak something...");
            try {
                startActivityForResult(intent, SPEECH_RECOGNITION_CODE);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getContext(),
                        "Sorry! Speech recognition is not supported in this device.",
                        Toast.LENGTH_SHORT).show();
        }
    }

    //share Data
    private void shareData() {
        StringBuilder stringBuilder = new StringBuilder("Truyện: " + nameStory + "\n" + chapter.getNameChapter());
        stringBuilder.append("\n" + chapter.getContentOfChapter());
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Truyện Kium Dung");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, stringBuilder.toString());
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        int position = getPositionOfChapter(chapter);
        switch (v.getId()) {
            case R.id.imgMoreRead:
                Animation animFadein = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                imgMoreRead.startAnimation(animFadein);
                showMoreMenu();
                break;
            case R.id.imgBack2:
                Animation animFade = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                imgBackRead.startAnimation(animFade);
//                getActivity().onBackPressed();
                ReadActivity a = (ReadActivity) getActivity();
                a.replaceFragmentContent(new ChapterFragment());
                FragmentManager fmgr = a.getSupportFragmentManager();
                fmgr.popBackStack();
                break;
            case R.id.rlIncreaseTextSize:
                Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                rlIncreaseTextSize.startAnimation(anim);
                if (TEXT_SIZE_OF_CONTENT_CHAPTER < 23) TEXT_SIZE_OF_CONTENT_CHAPTER += 1;
                if (TEXT_SIZE_OF_NAME_CHAPTER < 34) TEXT_SIZE_OF_NAME_CHAPTER += 1;
                tvContentChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_CONTENT_CHAPTER);
                saveTextSizeInPreferences(KEY_CONTENT_CHAPTER, TEXT_SIZE_OF_CONTENT_CHAPTER);
                tvNameChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_NAME_CHAPTER);
                saveTextSizeInPreferences(KEY_NAME_CHAPTER, TEXT_SIZE_OF_NAME_CHAPTER);
                break;
            case R.id.rlDecreaseTextSize:
                Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                rlDecreaseTextSize.startAnimation(ani);
                if (TEXT_SIZE_OF_CONTENT_CHAPTER > 9) TEXT_SIZE_OF_CONTENT_CHAPTER -= 1;
                if (TEXT_SIZE_OF_NAME_CHAPTER > 20) TEXT_SIZE_OF_NAME_CHAPTER -= 1;
                tvContentChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_CONTENT_CHAPTER);
                tvNameChapter.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE_OF_NAME_CHAPTER);
                saveTextSizeInPreferences(KEY_CONTENT_CHAPTER, TEXT_SIZE_OF_CONTENT_CHAPTER);
                saveTextSizeInPreferences(KEY_NAME_CHAPTER, TEXT_SIZE_OF_NAME_CHAPTER);
                break;
            case R.id.rlSearch:
                Animation an = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                rlSearch.startAnimation(an);
                showDialogSearch();
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                break;
            case R.id.rlNextChapter:
                //next Chapter
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                rlNextChapter.startAnimation(animation);
                if (position != -1 && position != listChapter.size() - 1) {
                    chapter = listChapter.get(position + 1);
                    scrollView.fullScroll(ScrollView.FOCUS_UP);
                    String nameChapter = getNameChapter(chapter);
                    tvTitelNameStory.setText(getNumberChapter(chapter));
                    tvNameChapter.setText(nameChapter);
                    tvContentChapter.setText(chapter.getContentOfChapter());
//                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                break;
            case R.id.rlPreviousChapter:
                //pre Chpater
                Animation animatio = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                rlPreviousChapter.startAnimation(animatio);
                if (position != -1 && position != 0) {
                    chapter = listChapter.get(position - 1);
                    scrollView.fullScroll(ScrollView.FOCUS_UP);
                    String nameChapter = getNameChapter(chapter);
                    tvTitelNameStory.setText(getNumberChapter(chapter));
                    tvNameChapter.setText(nameChapter);
                    tvContentChapter.setText(chapter.getContentOfChapter());
//                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                break;
            case R.id.imgShare:
                Animation aniShare = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                imgShare.startAnimation(aniShare);
                shareData();
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SPEECH_RECOGNITION_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = result.get(0);
                    new TextClass(getContext(), SEARCH_TYPE).execute(text);
                }
                break;
        }
    }

    class TimeRunable implements Runnable {

        private Handler timerHandler;

        public TimeRunable(Handler timerHandler) {
            this.timerHandler = timerHandler;
        }

        @Override
        public void run() {
            scrollView.smoothScrollBy(0, 5);
            timerHandler.postDelayed(this, 60);
        }
    }

    class TextClass extends AsyncTask<String, Void, Void> {

        Context context;
        int type;
        ArrayList<Integer> arrIndexOfKeyWord;
        ArrayList<String> arrErrorText;
        String text;

        public TextClass(Context context, int type) {
            this.context = context;
            this.type = type;
            arrIndexOfKeyWord = new ArrayList<>();
            arrErrorText = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(String... params) {
            text = params[0];
            if (type == CHECK_SPELL_TYPE) {
                arrErrorText = checkSpelling(text);
            } else if (type == SEARCH_TYPE) {
                Log.e("ST Query1", text + "  " + convertUnsigned.convertString(text));
                arrIndexOfKeyWord = getArrayIndexOfQueryText(convertUnsigned.convertString(text));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (type == SEARCH_TYPE) {
                if (arrIndexOfKeyWord.isEmpty()) {
                    Toast.makeText(getContext(), "Không có kết quả phù hợp", Toast.LENGTH_SHORT).show();
                } else {
                    scrollToPosition(text);
                    highlightText(arrIndexOfKeyWord, text);
                }
            } else if (type == CHECK_SPELL_TYPE) {
                showDialogCheckSpell(arrErrorText);
            }
            progressDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Đang tìm kiếm...");
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

}
