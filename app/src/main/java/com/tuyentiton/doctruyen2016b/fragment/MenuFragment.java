package com.tuyentiton.doctruyen2016b.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tuyentiton.doctruyen2016b.AppPreference;
import com.tuyentiton.doctruyen2016b.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Tuyen Ti Ton on 2/16/2017.
 */

public class MenuFragment extends Fragment implements View.OnClickListener {

    //Ảnh đại diện
    private ImageView imgAvatar, imgCover, imgUpdateAvatar;
    private Uri uri;
    private final int CAMERA_CODE = 0;
    private final int GALLEY_CODE = 1;
    private final int CROP_CODE = 2;
    private final String KEY_AVATAR = "Avatar";
    private final String KEY_COVER = "Cover";
    private TextView tvNameAccount, tvEmailAccount;

    public static final int MENU_HOME = 0;
    public static final int MENU_FAVORITE = 1;
    public static final int MENU_SETTING = 2;
    public static final int MENU_INTRO = 3;
    public static final int MENU_LOGIN = 4;
    private Position position;

    RelativeLayout naviHome, naviLike, naviIntro, naviSetting, naviLogin;
    private AppPreference appPreference;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu, container, false);
        appPreference = new AppPreference();
        appPreference.init(getContext());
        naviHome = (RelativeLayout) rootView.findViewById(R.id.naviHome);
        naviHome.setOnClickListener(this);
        naviLike = (RelativeLayout) rootView.findViewById(R.id.naviLike);
        naviLike.setOnClickListener(this);
        naviIntro = (RelativeLayout) rootView.findViewById(R.id.naviIntro);
        naviIntro.setOnClickListener(this);
        naviSetting = (RelativeLayout) rootView.findViewById(R.id.naviSetting);
        naviSetting.setOnClickListener(this);
        naviLogin = (RelativeLayout) rootView.findViewById(R.id.naviLogin);
        naviLogin.setOnClickListener(this);
        imgAvatar = (ImageView) rootView.findViewById(R.id.imgAvatar);
        imgAvatar.setOnClickListener(this);
        imgCover = (ImageView) rootView.findViewById(R.id.imgCover);
        imgUpdateAvatar = (ImageView) rootView.findViewById(R.id.imgUpdateImage);
        imgUpdateAvatar.setOnClickListener(this);
        tvNameAccount = (TextView) rootView.findViewById(R.id.tvUserName);
        tvEmailAccount = (TextView) rootView.findViewById(R.id.tvEmailOfUserName);
        String nameAccount = appPreference.getName();
        String emailAccount = appPreference.getEmail();
        if (nameAccount != null && emailAccount != null) {
            tvNameAccount.setText(nameAccount);
            tvEmailAccount.setText(emailAccount);
        } else {
            tvNameAccount.setText("Name Account");
            tvEmailAccount.setText("emailofaccount@gmail.com");
        }
        String path = appPreference.getPath();
        if (path != null) {
            Picasso.with(getContext()).load(Uri.parse(path)).into(imgAvatar);
            Picasso.with(getContext()).load(Uri.parse(path)).into(imgCover);
//            File  file = new File(path);
//            if (file.exists()){
//                Log.d("__t", path);
//                imgAvatar.setImageBitmap(getBitmapFromPath(path));
//                imgCover.setImageBitmap(getBitmapFromPath(path));
//            } else {
//                imgAvatar.setImageDrawable(getResources().getDrawable(R.drawable.logo));
//                imgCover.setImageDrawable(getResources().getDrawable(R.drawable.logo));
//            }
        } else {
            imgAvatar.setImageDrawable(getResources().getDrawable(R.drawable.logo));
            imgCover.setImageDrawable(getResources().getDrawable(R.drawable.logo));
        }
        return rootView;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        String path = appPreference.getPath();
//        if (path != null) {
//            Log.d("__t", path);
//            imgAvatar.setImageBitmap(getBitmapFromPath(path));
//            imgCover.setImageBitmap(getBitmapFromPath(path));
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            position = (Position) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        position = null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.naviHome:
                setChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviLogin);
                setNoChooseBackgroundForMenu(naviSetting);
                position.posionForNavi(MENU_HOME);
                break;
            case R.id.naviLike:
                setChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviLogin);
                position.posionForNavi(MENU_FAVORITE);
                break;
            case R.id.naviIntro:
                setChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviLogin);
                position.posionForNavi(MENU_INTRO);
                break;
            case R.id.naviSetting:
                setChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviLogin);
                position.posionForNavi(MENU_SETTING);
                showDialogSetting();
                break;
            case R.id.naviLogin:
                setChooseBackgroundForMenu(naviLogin);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviHome);
                position.posionForNavi(MENU_LOGIN);
                break;
            case R.id.imgAvatar:
                getImageFromGalley();
//                checkReadPermission();
//                menuUpdateAvatar();
                break;
            case R.id.imgUpdateImage:
                getImageFromGalley();
//                checkReadPermission();
//                menuUpdateAvatar();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == 111) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                checkWritePermission();
//            }
//        }
//
//        if (requestCode == 222) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                menuUpdateAvatar();
//            }
//        }
//
//        if(requestCode == 333){
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                getImageFromCamera();
//            }
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
//            case CAMERA_CODE:
//                if (resultCode == RESULT_OK) {
//                    cropImage();
//                }
//                break;
            case GALLEY_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    uri = data.getData();
                    Log.e("Uri", String.valueOf(uri));
                    Picasso.with(getContext()).load(uri).into(imgAvatar);
                    Picasso.with(getContext()).load(uri).into(imgCover);
                    appPreference.setPathImage(String.valueOf(uri));
//                    try {
//                        final InputStream imageStream = getActivity().getContentResolver().openInputStream(uri);
//                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                        imgAvatar.setImageBitmap(selectedImage);
//                        imgCover.setImageBitmap(selectedImage);
//                        appPreference.setPathImage(String.valueOf(uri));
////                        cropImage();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
//                    }
                }
                break;
            case CROP_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    Bundle bundle = data.getExtras();
                    Bitmap bitmap = bundle.getParcelable("data");
//                    String path = getPath(bitmap);
//                    Log.d("__t", path);
//                    appPreference.setPathImage(path);
                    imgAvatar.setImageBitmap(bitmap);
                    imgCover.setImageBitmap(bitmap);
                }
                break;
        }
    }

//    public Bitmap getBitmapFromPath(String path) {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
//        return bitmap;
//    }
//
//    public String getPath(Bitmap bitmap) {
//        Date now = new Date();
//        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
//        String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";
//        File imageFile = new File(mPath);
//        FileOutputStream outputStream = null;
//        try {
//            outputStream = new FileOutputStream(imageFile);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        int quality = 100;
//        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
//        try {
//            outputStream.flush();
//            outputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return mPath;
//    }

//    //menu update Avatar
//    private void menuUpdateAvatar() {
//        PopupMenu menu = new PopupMenu(getContext(), imgUpdateAvatar);
//        menu.inflate(R.menu.menu_update_image);
//        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                if (item.getItemId() == R.id.mnuFromCamera) {
//                    getImageFromCamera();
//                } else if (item.getItemId() == R.id.mnuFromGalley) {
//                    getImageFromGalley();
//                }
//                return false;
//            }
//        });
//        menu.show();
//    }

////    Intent Crop ảnh
//    private void cropImage() {
//        Log.d("__", "yep");
//        try {
//            Intent cropIntent = new Intent("com.android.camera.action.CROP");
//            cropIntent.setDataAndType(uri, "image/*");
//            cropIntent.putExtra("crop", "true");
//            cropIntent.putExtra("outputX", 180);
//            cropIntent.putExtra("outputY", 180);
//            cropIntent.putExtra("aspectX", 3);
//            cropIntent.putExtra("aspectY", 4);
//            cropIntent.putExtra("scaleUpIfNeeded", true);
//            cropIntent.putExtra("return-data", true);
//            startActivityForResult(cropIntent, CROP_CODE);
//        } catch (Exception e) {
//
//        }
//    }

    //Intent lấy ảnh từ thư viện ảnh
    private void getImageFromGalley() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLEY_CODE);
    }

//    //Intent lấy ảnh từ Camera
//    private void getImageFromCamera() {
////        checkCameraPermission();
//        Intent camIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        File file = new File(Environment.getExternalStorageDirectory(), "file" + String.valueOf(System.currentTimeMillis()) + ".jpg");
//        uri = Uri.fromFile(file);
//        camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        camIntent.putExtra("return-data", true);
//        startActivityForResult(camIntent, CAMERA_CODE);
////    }

    private void showDialogSetting() {
        String name = appPreference.getName();
        String email = appPreference.getEmail();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customer_dialog_setting);
        dialog.show();
        final EditText edtNameAccount = (EditText) dialog.findViewById(R.id.edtNameAccount);
        if (name != null)
            edtNameAccount.setText(name);
        final EditText edtEmailAccount = (EditText) dialog.findViewById(R.id.edtEmailAccount);
        if (email != null)
            edtEmailAccount.setText(email);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCanelDialog);
        btnOk.setOnClickListener(new View.OnClickListener() {
            String finalName;
            String finalEmail;

            @Override
            public void onClick(View v) {
                if (edtNameAccount.getText().toString().isEmpty()) {
                    edtNameAccount.setError("Chưa điền tên");
                    edtNameAccount.requestFocus();
                    return;
                } else {
                    if (edtEmailAccount.getText().toString().isEmpty()) {
                        edtEmailAccount.setError("Chưa điền email");
                        edtEmailAccount.requestFocus();
                        return;
                    } else {
                        finalName = edtNameAccount.getText().toString();
                        finalEmail = edtEmailAccount.getText().toString();
                    }
                }
                tvNameAccount.setText(finalName);
                tvEmailAccount.setText(finalEmail);
                appPreference.setUser(finalName, finalEmail);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setChooseBackgroundForMenu(RelativeLayout relativeLayout) {
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.menuNavi));
    }

    private void setNoChooseBackgroundForMenu(RelativeLayout relativeLayout) {
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.frmMenu));
    }

    private void showToast(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    public interface Position {
        void posionForNavi(int position);
    }

//    void checkReadPermission() {
//        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    getActivity(),
//                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                    111);
//            return;
//        }
//    }
//
//    void checkWritePermission() {
//        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    getActivity(),
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    222);
//            return;
//        }
//    }
//
//    void checkCameraPermission() {
//        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    getActivity(),
//                    new String[]{Manifest.permission.CAMERA},
//                    333);
//            return;
//        }
//    }

}
