/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.constant;

/**
 *
 * @author Tuyen Ti Ton
 *
 */
public class Constant {

    public static final String CONSONANT = "trhbmvđnlxpsdcqkg";
    public static final String DOUBLE_CONSONANT[] = {"tr", "th", "ph", "gh", "kh", "ch", "nh", "ng"};
    public static final String DOUBLE_VOWEL[] = {"oa", "ai", "oe", "ưa", "ươ", "uâ", "au", "ia", "êu", "uô", "ua", "iê", "oc"};
    public static final String FIRST_CONSONANT = "qvbdlksxrđ";
    public static final String VOWEL ="aieêăâơôưuoy";
    
}
