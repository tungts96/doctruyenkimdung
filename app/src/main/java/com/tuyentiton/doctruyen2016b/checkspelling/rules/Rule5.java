/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule5 extends Rule{
    String x = null;
    //Trong tiếng Việt, không phải nguyên âm nào cũng đứng trước t, c, p, ch được.
    //Hãy liệt kê các nguyên âm được phép đứng trước chúng. => Chỉ có các từ có dấu sắc hoặc dấu nặng được phép.
    String voewl[] = {"ạ", "á", "ị", "í", "ẹ", "é", "ệ", "ế"};
    public boolean checkValid(String x){
        this.x = x;
        return true;
    }

    public String show(){
        return "Rule5 Error -->" + x;
    }
}
