package com.tuyentiton.doctruyen2016b.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.tuyentiton.doctruyen2016b.interfaces.NoticeDialogListener;

/**
 * Created by Son tung on 3/25/2017.
 */

public class NoticeDialog extends DialogFragment {

    private String title, message;
    private Context context;
    NoticeDialogListener mListener;

    public NoticeDialog(String title, String message, Context context) {
        this.title = title;
        this.message = message;
        this.context = context;
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        try {
//            mListener = (NoticeDialogListener) context;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
//                .setNegativeButton("ss", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        mListener.onDialogNegativeClick(NoticeDialog.this);
//                    }
//                })
//                .setNeutralButton("sss", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        mListener.onDialogNeutralClick(NoticeDialog.this);
//                    }
//                });
        Dialog d = builder.create();
        d.show();
    }
}
