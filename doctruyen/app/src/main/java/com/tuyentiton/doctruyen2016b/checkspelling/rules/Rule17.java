/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule17 extends Rule {

    String x = null;
    //Các nguyên âm được phép sau "e", "é", "è", "ẻ", "ẹ", "ẽ" thì chỉ có: "o"

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("e") || x1.equalsIgnoreCase("é") || x1.equalsIgnoreCase("è") || x1.equalsIgnoreCase("ẻ")
                    || x1.equalsIgnoreCase("ẹ") || x1.equalsIgnoreCase("ẽ")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!x2.equalsIgnoreCase("o")) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule17 Error -->" + x);
    }
}
