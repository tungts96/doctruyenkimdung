package com.tuyentiton.doctruyen2016b.model;

import java.util.Comparator;

/**
 * Created by Tuyen Ti Ton on 5/8/2017.
 */

public class MyComparator implements Comparator<Chapter> {
    @Override
    public int compare(Chapter o1, Chapter o2) {
        if (o1 == null && o2 == null) {
            return 0;
        }

        if (o1 == null) {
            return -1;
        }

        if (o2 == null) {
            return 1;
        }

        int value = o1.getValue() - o2.getValue();
        if (value != 0) {
            return value;
        }

        value = String.valueOf(o1.getIdChapter()).compareTo(String.valueOf(o2.getIdChapter()));
        return value;
    }
}
