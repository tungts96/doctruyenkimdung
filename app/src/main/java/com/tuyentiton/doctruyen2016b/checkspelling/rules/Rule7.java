/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule7 extends Rule {

    String x = null;

    // Trong tiếng Việt có một số phụ âm không được đứng cuối từ: q v b d l k s x r đ
    public boolean checkValid(String x) {
        this.x = x;
        String consonants = Constant.FIRST_CONSONANT;
        if (consonants.contains(x.charAt(x.length() - 1) + "")) {
            return false;
        }
        return true;
    }

    public String show(){
        return "Rule7 Error -->" + x;
    }
}
