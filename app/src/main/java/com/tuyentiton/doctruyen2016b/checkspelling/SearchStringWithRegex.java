package com.tuyentiton.doctruyen2016b.checkspelling;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tuyentiton.doctruyen2016b.database.KimDungSQLite;
import com.tuyentiton.doctruyen2016b.model.Chapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static com.tuyentiton.doctruyen2016b.database.KimDungSQLite.DATABASE_TABLE_STKIMDUNG;

/**
 * Created by Son tung on 4/27/2017.
 */

public class SearchStringWithRegex {

    private Context context;

    public SearchStringWithRegex(Context context) {
        this.context = context;
    }

    public ArrayList<Chapter> actionSearch(int idStory, String key) {
        ArrayList<Chapter> chapters = getChapters(idStory);
        ArrayList<Chapter> resultSearch = new ArrayList<>();
        for (int i = 0; i < chapters.size(); i++) {
            resultSearch.addAll(getValuesOfChapters(key, chapters.get(i), 5));
        }
        return resultSearch;
    }

    private ArrayList<Chapter> getChapters(int idStory) {
        ArrayList<Chapter> chapters = new ArrayList<>();
        chapters.clear();
        SQLiteDatabase database = KimDungSQLite.initDatabase(context);
        Cursor c = database.rawQuery("Select * from " + DATABASE_TABLE_STKIMDUNG + " where stID=" + idStory, null);
        while (c.moveToNext()) {
            int idChapter = c.getInt(0);
            String nameChapter = c.getString(1);
            String contentOfChapter = c.getString(3);
            contentOfChapter = contentOfChapter.replaceAll("<br/>", "\n");
            contentOfChapter = contentOfChapter.replaceAll("<p>", "");
            contentOfChapter = contentOfChapter.replaceAll("<br />", "\n");
            Chapter chapter = new Chapter(idChapter, nameChapter, contentOfChapter);
            chapters.add(chapter);
        }
        c.close();
        return chapters;
    }

    private int getScoreOfValue(ArrayList<Integer> listIndex) {
        int score = 0;
        // kiem tra xem cum tu co khop hay k;
        int check = 0;
        for (int i = 0; i < listIndex.size() - 1; i++) {
            if (listIndex.get(i) + 1 == listIndex.get(i + 1)) {
                check++;
            } else {
                break;
            }
        }

        if (check == listIndex.size() - 1) return 0;

        // Mỗi gtri -1 thì cong 50 điểm: tức là ký tự k có trong cụm từ tìm đk
        for (int i = 0; i < listIndex.size(); i++) {
            if (listIndex.get(i) == -1) {
                score += 50;
            }
        }

        if (score == (listIndex.size() - 1) * 50) return Integer.MAX_VALUE - 10000;

        // moi tu trong khoa cach nhau n vi tri thi diem dk cong them n
        for (int i = 0; i < listIndex.size(); i++) {
            if (listIndex.get(i) != -1) {
                for (int j = i + 1; j <= listIndex.size() - 1; j++) {
                    if (listIndex.get(j) != -1) {
                        int a = listIndex.get(i);
                        int b = listIndex.get(j);
                        if (a > b) {
                            // cộng khoảng cách
                            score += (a - b);
                        } else {
                            score += (b - a);
                        }
                    }
                }
            }
        }


        return score;
    }

    private ArrayList<Chapter> getValuesOfChapters(String key, Chapter chapter, int range) {
        String nameChapter = chapter.getNameChapter();
        key = key.replaceAll("[()?:!.,;{}]+", " ");
        ArrayList<String> keys = new ArrayList<>(Arrays.asList(key.split(" ")));
        ArrayList<String> rootContent = new ArrayList<>(Arrays.asList(chapter.getContentOfChapter().split(" ")));
        String contents = chapter.getContentOfChapter().replaceAll("[<>()?:!.,;{}]+", "");
        Log.d("__t", "----------------------------" + chapter.getIdChapter() + "--------------------------------");
        ArrayList<Chapter> listChapters = new ArrayList<>();
        ArrayList<Integer> listIndexOfKey = new ArrayList<>();
        ArrayList<String> mainContent = new ArrayList<>(Arrays.asList(contents.split(" ")));
        ArrayList<String> cloneContent = new ArrayList<>(Arrays.asList(contents.split(" ")));
        mainContent.add("abczxy");
        int j = 0;
        int old = 0;
        int position = 0;
        boolean exits = false;
        int firstPosition = -1;
        while (j < mainContent.size()) {

            // giả sử từ đầu tiên trong khóa làm mainkey
            if (mainContent.get(j).trim().equalsIgnoreCase(keys.get(position).trim())) {
                if (firstPosition == -1) {
                    firstPosition = j;
                }
                exits = true;
                if (old != j) {
                    // nếu tìm đk từ trùng mainkey thì reset lại cloneContent
                    cloneContent = new ArrayList<>(Arrays.asList(contents.split(" ")));
                    Log.d("__tReset", "resetList");
                }
                old = j;
                listIndexOfKey = new ArrayList<>();
                listIndexOfKey.add(j);
                for (int x = 0; x < keys.size(); x++) {
                    if (keys.get(x).trim().equalsIgnoreCase(keys.get(position).trim())) {
                        continue;
                    }

                    // gioi han toi da cho phep là 5 từ trong khóa
                    int start;
                    if (j > range) {
                        start = j - range;
                    } else {
                        start = 0;
                    }

                    // vi tri cuoi cung cua day tim kiem

                    int end = start + keys.size() * range;

                    if (end >= mainContent.size()) {
                        end = mainContent.size() - 1;
                    }
                    for (int y = start; y < end; y++) {

                        if (keys.get(x).trim().equalsIgnoreCase(cloneContent.get(y).trim())) {
                            // Thêm vào vị trí y của key trong chapter
                            int distances = 0;
                            if (j < y) {
                                distances = y - j;
                            } else {
                                distances = j - y;
                            }

                            if (distances <= range) {
                                listIndexOfKey.add(y);
                            } else {
                                listIndexOfKey.add(-1);
                            }
                            cloneContent.set(y, cloneContent.get(y) + "rm");
                            break;
                        } else if (y == end - 1 && !keys.get(x).trim().equalsIgnoreCase(cloneContent.get(y).trim())) {
                            // TH từ key k có trong chapter thì thêm vị trí key là -1
                            listIndexOfKey.add(-1);
                        }

                    }
                }
                Log.d("__tindex", "index:" + listIndexOfKey.toString());
                if (getScoreOfValue(listIndexOfKey) == 0) {
                    Log.d("__t", "equal 100%");
                    listChapters.add(new Chapter(chapter.getIdChapter(), nameChapter, chapter.getContentOfChapter(), 0, getResultAfterSearch(rootContent, listIndexOfKey, firstPosition)));
                    return listChapters;
                } else if (getScoreOfValue(listIndexOfKey) != Integer.MAX_VALUE - 10000) {
                    Log.d("__tScore", "score: " + getScoreOfValue(listIndexOfKey));
                    listChapters.add(new Chapter(chapter.getIdChapter(), nameChapter, chapter.getContentOfChapter(), getScoreOfValue(listIndexOfKey), getResultAfterSearch(rootContent, listIndexOfKey, firstPosition)));
                }
                int check = 1;
                for (int k = 1; k < listIndexOfKey.size(); k++) {
                    if (listIndexOfKey.get(k) == -1) check++;
                }
                // nếu đã xét hết 1 lượt các TH tại vị trí trùng mainkey thì xét tiếp sang vị trí tiếp theo
                // nếu chưa hết TH thì tiếp tục lấy vị trí hiện tại làm trung tâm
                if (check == listIndexOfKey.size()) {
                    j++;
                }

            } else {
                // nếu vị trí đang xét k trùng mainkey thì tăng vị trí lên
                j++;
                if (j == mainContent.size() - 1) {
                    if (position < keys.size() - 1) {
                        if (!exits || listChapters.size() == 0) {
                            // nếu mainkey hiện tại k có trong chap thì xét mainkey là từ tiếp theo trong khóa
                            Log.d("__tNewKey", keys.get(position + 1));
                            j = 0;
                            position++;
                        }
                    }

                }

            }
        }
        //lay values co diem nho nhat va tra ve value do
        if (listChapters.size() == 0) {
            listChapters.add(new Chapter(chapter.getIdChapter(), nameChapter, chapter.getContentOfChapter(), Integer.MAX_VALUE, getResultAfterSearch(rootContent, listIndexOfKey, firstPosition)));
        }
        return listChapters;
    }

    private Chapter getBestValueOfChapter(ArrayList<Chapter> chapters) {
        int best = -1;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < chapters.size(); i++) {
            if (chapters.get(i).getValue() < min) {
                min = chapters.get(i).getValue();
                best = i;
            }
        }
        return chapters.get(best);
    }

    private String getResultAfterSearch(ArrayList<String> contents, ArrayList<Integer> listIndex, int firstPosition) {
        if (listIndex.size() == 0) {
            return "";
        }

        int max = Collections.max(listIndex);
        int min = max;
        int end;
        for (int i = 0; i < listIndex.size(); i++) {
            if (listIndex.get(i) != -1) {
                if (listIndex.get(i) < min) {
                    min = listIndex.get(i);
                }
            }
        }
        // min = max tuc la ket qua chi co 1 tu trung tu khoa
        // lay ket qua dau tien tim duoc
        if (min == max) {
            min = firstPosition;
            end = min + listIndex.size() + 5;
        } else {
            end = max + 6;

        }
        if (max == listIndex.size() - 1) {
            end = max;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = min; i <= end; i++) {
            if (end == contents.size() - 1) break;
            builder.append(contents.get(i).trim() + " ");
        }
        Log.d("__tresult", builder.toString());
        return builder.toString().trim();
    }


}
