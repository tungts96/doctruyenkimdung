/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule19 extends Rule {

    String x = null;
    // Nguyên âm "ễu" chỉ được phép đi trong từ "Tễu" và "phễu"

    public boolean checkValid(String x) {
        this.x = x;
        if (x.contains("ễu")) {
            int position = x.indexOf("ễ");
            String x2 = x.charAt(position - 1) + "";
            if (x2.equalsIgnoreCase("i")) {
                return true;
            }
            if (!x.equalsIgnoreCase("tễu")) {
                if (!x.equalsIgnoreCase("phễu")) {
                    return false;
                }
            }
        }
        return true;
    }

    public void show() {
        System.out.println("Rule19 error -->" + x);
    }
}
