package com.tuyentiton.doctruyen2016b.model;

import java.io.Serializable;

/**
 * Created by Son tung on 3/14/2017.
 */

public class Chapter implements Serializable {
    private int idStory;
    private int idChapter;
    private String nameChapter;
    private String contentOfChapter;
    private String timeRead;
    private int value;
    private String result;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Chapter() {
    }

    public Chapter(int idChapter, String nameChapter, String contentOfChapter, int value, String result) {
        this.idChapter = idChapter;
        this.nameChapter = nameChapter;
        this.contentOfChapter = contentOfChapter;
        this.value = value;
        this.result = result;
    }



    public Chapter(int idChapter, int idStory, String nameChapter, String contentOfChapter) {
        this.idChapter = idChapter;
        this.idStory = idStory;
        this.nameChapter = nameChapter;
        this.contentOfChapter = contentOfChapter;
    }

    public Chapter(int idChapter,String nameChapter, String contentOfChaptere) {
        this.idChapter = idChapter;
        this.contentOfChapter = contentOfChaptere;
        this.nameChapter = nameChapter;
    }

    public String getContentOfChapter() {
        return contentOfChapter;
    }

    public void setContentOfChapter(String contentOfChapter) {
        this.contentOfChapter = contentOfChapter;
    }

    public int getIdChapter() {
        return idChapter;
    }

    public void setIdChapter(int idChapter) {
        this.idChapter = idChapter;
    }

    public int getIdStory() {
        return idStory;
    }

    public void setIdStory(int idStory) {
        this.idStory = idStory;
    }

    public String getNameChapter() {
        return nameChapter;
    }

    public void setNameChapter(String nameChapter) {
        this.nameChapter = nameChapter;
    }

    public String getTimeRead() {
        return timeRead;
    }

    public void setTimeRead(String timeRead) {
        this.timeRead = timeRead;
    }
}
