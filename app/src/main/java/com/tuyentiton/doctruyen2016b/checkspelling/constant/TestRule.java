/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.constant;

import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule1;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule10;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule11;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule12;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule13;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule14;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule15;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule16;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule17;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule18;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule19;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule2;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule20;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule21;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule22;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule23;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule24;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule25;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule26;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule27;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule28;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule29;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule3;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule4;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule5;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule6;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule7;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule8;
import com.tuyentiton.doctruyen2016b.checkspelling.rules.Rule9;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Tuyen Ti Ton
 */
public class TestRule {

    private char[] charA = {'à', 'á', 'ạ', 'ả', 'ã',// 0-&gt;16
            'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ'};// a,// ă,// â
    private char[] charE = {'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ',// 17-&gt;27
            'è', 'é', 'ẹ', 'ẻ', 'ẽ'};// e
    private char[] charI = {'ì', 'í', 'ị', 'ỉ', 'ĩ'};// i 28-&gt;32
    private char[] charO = {'ò', 'ó', 'ọ', 'ỏ', 'õ',// o 33-&gt;49
            'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ',// ô
            'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ'};// ơ
    private char[] charU = {'ù', 'ú', 'ụ', 'ủ', 'ũ',// u 50-&gt;60
            'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ'};// ư
    private char[] charY = {'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ'};// y 61-&gt;65
    private char[] charD = {'đ', ' '}; // 66-67
    private char[][] CH = {charA, charE, charI, charO, charU, charY, charD};
    private String charact;

    Rule[] allRules;

    public TestRule() {
        // TODO code application logic here
        allRules = new Rule[29];
        allRules[0] = new Rule1();
        allRules[1] = new Rule2();
        allRules[2] = new Rule3();
        allRules[3] = new Rule4();
        allRules[4] = new Rule5();
        allRules[5] = new Rule6();
        allRules[6] = new Rule7();
        allRules[7] = new Rule8();
        allRules[8] = new Rule9();
        allRules[9] = new Rule10();
        allRules[10] = new Rule11();
        allRules[11] = new Rule12();
        allRules[12] = new Rule13();
        allRules[13] = new Rule14();
        allRules[14] = new Rule15();
        allRules[15] = new Rule16();
        allRules[16] = new Rule17();
        allRules[17] = new Rule18();
        allRules[18] = new Rule19();
        allRules[19] = new Rule20();
        allRules[20] = new Rule21();
        allRules[21] = new Rule22();
        allRules[22] = new Rule23();
        allRules[23] = new Rule24();
        allRules[24] = new Rule25();
        allRules[25] = new Rule26();
        allRules[26] = new Rule27();
        allRules[27] = new Rule28();
        allRules[28] = new Rule29();

        charact = String.valueOf(charA, 0, charA.length)
                + String.valueOf(charE, 0, charE.length)
                + String.valueOf(charI, 0, charI.length)
                + String.valueOf(charO, 0, charO.length)
                + String.valueOf(charU, 0, charU.length)
                + String.valueOf(charY, 0, charY.length)
                + String.valueOf(charD, 0, charD.length);
    }

    private int GetASCIIChar(char pC) {
        if ((int) pC == 32) {
            return ' ';
        }

        char tam = pC;// Character.toLowerCase(pC);

        int i = 0;
        while (i < charact.length() && charact.charAt(i) != tam) {
            i++;
        }
        if (i < 0 || i > 67)
            return pC;

        if (i == 66) {
            return 100;
        }
        if (i >= 0 && i <= 16) {
            return 97;
        }
        if (i >= 17 && i <= 27) {
            return 101;
        }
        if (i >= 28 && i <= 32) {
            return 105;
        }
        if (i >= 33 && i <= 49) {
            return 111;
        }
        if (i >= 50 && i <= 60) {
            return 117;
        }
        if (i >= 61 && i <= 65) {
            return 121;
        }
        return pC;
    }

    public ArrayList<String> checkSpelling(String str){
        ArrayList arrErrorText = new ArrayList();
        StringTokenizer s = new StringTokenizer(str);
        while(s.hasMoreTokens()) {
            String text = s.nextToken();
            text = text.toLowerCase();
            //System.out.println(text);
            int dau = text.charAt(0);        Log.e("checkSpell",dau+" "+"1");
            int cuoi = text.charAt(text.length()-1);        Log.e("checkSpell",cuoi+" "+"2");
            dau = GetASCIIChar((char) dau);
            cuoi = GetASCIIChar((char) cuoi);
            while (cuoi < 48 || cuoi>122 || ((57< cuoi) && (cuoi<64)) || ((90<cuoi)  && (cuoi<96))){
                if (text.length() == 1){
                    text = "";
                    break;
                }
                text = text.substring(0,text.length()-1);   Log.e("checkSpell",text);
                cuoi = text.charAt(text.length()-1);
                cuoi = GetASCIIChar((char) cuoi);
                //System.out.println(cuoi);
            }
            //System.out.println(dau);
            Log.e("checkSpell",text.length()+"");
            if (text.length() > 0 ){
                while (dau < 48 || dau>122 || ((57< dau) && (dau<64)) || ((90<dau) && (dau<96))){
                    text = text.substring(1,text.length());
                    dau = text.charAt(0);
                    dau = GetASCIIChar((char) dau);
                }
            }
            //System.out.println(text);
            for (int i=0;i<allRules.length;i++){
                if (allRules[i] != null){
                    if (text.length() > 1 && !allRules[i].checkValid(text)) {
                        allRules[i].show();
                        arrErrorText.add(allRules[i].show());
                    }
                }
            }
        }
        return  arrErrorText;
    }

}
