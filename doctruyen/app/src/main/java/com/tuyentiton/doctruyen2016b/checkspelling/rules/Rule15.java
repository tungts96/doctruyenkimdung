/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule15 extends Rule {

    String x = null;
    // Chỉ có một số nguyên âm được phép đứng đằng sau "à" để tạo thành cặp nguyên âm. Hãy liệt kê: ài, àu, ào, ày.

    String temp = "iuoy";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("à")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!temp.contains(x2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule15 Error -->" + x);
    }

}
