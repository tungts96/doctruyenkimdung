/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule3 extends Rule {
    String x = null;
    //Nếu một từ có 2 phụ âm cạnh nhau thì các phụ âm đó phải là: tr, th, ph, gh, kh, ch, nh, ng,

    public boolean checkValid(String x) {
        this.x = x;
        String doubleConsonant[] = Constant.DOUBLE_CONSONANT;
        String consonant = Constant.CONSONANT;
        String consonants = null;
        int check =0;
        for (int i = 0; i < x.length(); i++) {
            check = 0;
            if (i < (x.length() - 1)) {
                String at_i = x.charAt(i) + "";
                String at_i1 = x.charAt(i + 1) + "";
                if (consonant.contains(at_i)) {
                    if (consonant.contains(at_i1)) {
                        consonants = at_i + at_i1;
                        for (int j = 0; j < doubleConsonant.length; j++) {
                            if (consonants.equalsIgnoreCase(doubleConsonant[j])) {
                                break;
                            } else {
                                check++;
                            }
                        }
                    }
                }
                if(check == doubleConsonant.length){
                    return false;
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule3 Error -->" + x);
    }
}
