package com.tuyentiton.doctruyen2016b.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Son tung on 3/4/2017.
 */

public class KimDungSQLite extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "kimdung.sqlite";
    public static final String DB_PATH_SUFFIX = "/databases/";
    public static final String DATABASE_TABLE_KIMDUNG = "kimdung";
    public static final String DATABASE_TABLE_STKIMDUNG = "st_kimdung";

    public KimDungSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static SQLiteDatabase initDatabase(Context context) {
        SQLiteDatabase database = null;
        try {
            String outFileName = context.getApplicationInfo().dataDir + DB_PATH_SUFFIX + DATABASE_NAME;
            File f = new File(outFileName);
            if (!f.exists()) {
                InputStream inputStream = context.getAssets().open(DATABASE_NAME);
                File folder = new File(context.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
                if (!folder.exists()) {
                    folder.mkdir();
                }
                FileOutputStream outputStream = new FileOutputStream(outFileName);
                byte[] buffer = new byte[1024];

                //chep 1024 byte cho den khi bang length
                int length;
                while ((length = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            }
            database = context.openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return database;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
