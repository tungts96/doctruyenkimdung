/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule22 extends Rule {

    String x = null;
    //KHÔNG nguyên âm được phép đứng đằng sau "ă" để tạo thành cặp nguyên âm. 
    String vowel = Constant.VOWEL;

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ă") || x1.equalsIgnoreCase("ằ") || x1.equalsIgnoreCase("ắ") || x1.equalsIgnoreCase("ẵ")
                    || x1.equalsIgnoreCase("ẳ") || x1.equalsIgnoreCase("ặ")) {
                String x2 = x.charAt(i + 1) + "";
                if (vowel.contains(x2)) {
                    return false;
                }
            }
        }
        return true;
    }

    public String show(){
        return "Rule22 Error -->" + x;
    }
}
