package com.tuyentiton.doctruyen2016b.interfaces;

/**
 * Created by Son tung on 4/18/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
