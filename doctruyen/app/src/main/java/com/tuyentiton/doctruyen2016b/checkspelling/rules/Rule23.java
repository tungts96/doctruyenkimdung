/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

import android.util.Log;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule23 extends Rule {

    String x = null;
    //Cặp nguyên âm "oo" chỉ có thể thuộc về xoong, boong, 

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            String x2 = x.charAt(i + 1) + "";
            String x3 = x1 + x2;
            if (x3.equalsIgnoreCase("oo")) {
                if (!x.equalsIgnoreCase("xoong")) {
                    if (!x.equalsIgnoreCase("boong")) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule23 Error -->" + x);
    }

}
