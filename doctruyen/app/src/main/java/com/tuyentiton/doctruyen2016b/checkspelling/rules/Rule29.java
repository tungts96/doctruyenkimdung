/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule29 extends Rule {

    String x = null;
    //Nguyên âm "ũ" đi sau là y được. Nhưng đi sau là "a" với các trường hợp "đũa", "dũa"

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ũ")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!x2.equalsIgnoreCase("y")) {
                        if (x2.equalsIgnoreCase("a")) {
                            if (!x.equalsIgnoreCase("đũa")) {
                                if (!x.equalsIgnoreCase("dũa")) {
                                    return false;
                                }
                            }
                        } 
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule29 Error -->" + x);
    }

}
