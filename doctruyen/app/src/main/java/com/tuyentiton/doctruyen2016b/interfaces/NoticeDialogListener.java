package com.tuyentiton.doctruyen2016b.interfaces;

import android.support.v4.app.DialogFragment;

/**
 * Created by Son tung on 3/25/2017.
 */

public interface NoticeDialogListener {
    void onDialogPositiveClick(DialogFragment dialog);
    void onDialogNegativeClick(DialogFragment dialog);
    void onDialogNeutralClick(DialogFragment dialog);
}
