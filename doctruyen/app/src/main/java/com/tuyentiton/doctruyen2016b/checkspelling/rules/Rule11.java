/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule11 extends Rule {

    String x = null;
    //Chỉ có một số nguyên âm được phép đứng đằng sau "ã" để tạo thành cặp nguyên âm. Hãy liệt kê: ãi, ão, ãy
    String temp = "ioy";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ã") || x1.equalsIgnoreCase("ạ") || x1.equalsIgnoreCase("ả")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2) && !x2.equalsIgnoreCase("u")) {
                    if (!temp.contains(x2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule11 Error -->" + x);
    }
}
