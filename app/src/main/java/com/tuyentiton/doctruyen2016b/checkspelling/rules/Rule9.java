/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule9 extends Rule {

    //Có một số nguyên âm không được đi sau "a" để tạo thành cặp nguyên âm,
    //hãy liệt kê chúng: tất cả những nguyên âm nào có dấu thì không được đi sau "a", aa, aô, aê, aư, aơ, aă, aâ, ae,
    String x = null;
    String temp = "aeêăâơôư";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length()-1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("a")) {
                String x2 = x.charAt(i + 1) + "";
                if (temp.contains(x2)) {
                    return false;
                }
            }
        }
        return true;
    }

    public String show(){
        return "Rule9 Error -->" + x;
    }
}
