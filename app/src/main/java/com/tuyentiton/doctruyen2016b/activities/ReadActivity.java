package com.tuyentiton.doctruyen2016b.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.fragment.ChapterFragment;
import com.tuyentiton.doctruyen2016b.fragment.ReadChapterFragment;
import com.tuyentiton.doctruyen2016b.model.Chapter;
import com.tuyentiton.doctruyen2016b.model.Story;

import java.util.ArrayList;

public class ReadActivity extends AppCompatActivity {

    private boolean search = false;
    private String nameStory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        search = getIntent().getBooleanExtra("SEARCH_OK", false);
        nameStory = getIntent().getStringExtra("NAMESTORY");
        Log.d("__t", search + "");
        replaceFragmentContent(new ChapterFragment());
    }

    public Story getStory() {
        Intent intent = getIntent();
        Story story = (Story) intent.getSerializableExtra("STORY");
        return story;
    }

    public void replaceFragmentContent(Fragment fragment) {
        if (fragment != null) {
            if (search) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("SEARCH_OK", search);
                fragment.setArguments(bundle);
            }

            FragmentManager fmgr = getSupportFragmentManager();
            FragmentTransaction ft = fmgr.beginTransaction();
            ft.replace(R.id.container_body, fragment);
            ft.commit();
        }
    }

    public void addFragmentContent(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fmgr = getSupportFragmentManager();
            FragmentTransaction ft = fmgr.beginTransaction();
            ft.add(R.id.container_body, fragment);
            ft.addToBackStack(fragment.getClass().getSimpleName());
            ft.commit();
        }
    }

}

