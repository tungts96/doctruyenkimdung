/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule27 extends Rule {

    String x = null;
    //Nguyên âm "u" đi đằng sau sẽ là các nguyên âm "i", "a", "y", "ê", "ở", "ế"
    //Nguyên âm "ý", "e" và "ơ", "á", "ả", "ạ" "à" cũng đi sau "u"
    //được nhưng đó phải là các từ "quý" "que", quơ, "huơ", "quá" quả, quạ, quà
    String temp = "iayêởếâ";
    String temp2 = "eơáảạàý";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("u")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if ((i + 2) < x.length()) {
                        String x3 = x.charAt(i + 2) + "";
                        if (Constant.CONSONANT.contains(x3) || x3.equalsIgnoreCase("i")) {
                            return true;
                        }
                    }
                    if (!temp.contains(x2)) {
                        if (temp2.contains(x2)) {
                            if (!x.equalsIgnoreCase("quý")) {
                                if (!x.equalsIgnoreCase("que")) {
                                    if (!x.equalsIgnoreCase("quơ")) {
                                        if (!x.equalsIgnoreCase("quá")) {
                                            if (!x.equalsIgnoreCase("quả")) {
                                                if (!x.equalsIgnoreCase("quạ")) {
                                                    if (!x.equalsIgnoreCase("quà")) {
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        System.out.println("Rule27 error -->" + x);
    }

}
