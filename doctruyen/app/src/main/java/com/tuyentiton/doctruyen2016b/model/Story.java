package com.tuyentiton.doctruyen2016b.model;

import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;

/**
 * Created by Son tung on 2/28/2017.
 */

public class Story implements Serializable{
    private int idStory;
    private String name;
    private String describe;
    private boolean like;
    private String image;
    private SQLiteDatabase database = null;

    public Story(){}

    public Story(int idStory, String name) {
        this.idStory = idStory;
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public int getIdStory() {
        return idStory;
    }

    public void setIdStory(int idStory) {
        this.idStory = idStory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
