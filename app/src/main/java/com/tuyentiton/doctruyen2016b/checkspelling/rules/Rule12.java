/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 * @author Tuyen Ti Ton
 */
public class Rule12 extends Rule {

    String x = null;
    //Các nguyên âm được phép sau nguyên âm "i": ia, iu, iê, iế, iệ, iể, iễ, iề,
    //Nguyên âm i được phép đi cùng "ữ", nhưng đó phải là từ "giữ" 
    //Nguyên âm i được phép đi cùng "ó", "ố", "ỗ", "ọ", "ỏ", "ò", "ấ", "ặ" 
    //nhưng đó phải là từ "gió" , "giống", "giỗ", "giọng", "giỏ", "giò", "giá", "giấc", "giặc", 
    String temp = "ieăâơôưoy";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("i")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (temp.contains(x2)) {
                        if (x.equalsIgnoreCase("giơ")) {
                            return true;
                        }
                        if (i == x.length() - 2) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ữ")) {
                        if (!x.equalsIgnoreCase("giữ")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ó")) {
                        if (!x.equalsIgnoreCase("gió")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ố")) {
                        if (!x.equalsIgnoreCase("giống")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ỗ")) {
                        if (!x.equalsIgnoreCase("giỗ")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ọ")) {
                        if (!x.equalsIgnoreCase("giọng")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ỏ")) {
                        if (!x.equalsIgnoreCase("giỏ")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ò")) {
                        if (!x.equalsIgnoreCase("giò")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ấ")) {
                        if (!x.equalsIgnoreCase("giấc")) {
                            return false;
                        }
                    }
                    if (x2.equalsIgnoreCase("ặ")) {
                        if (!x.equalsIgnoreCase("giặc")) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public String show(){
        return "Rule12 Error -->" + x;
    }
}
