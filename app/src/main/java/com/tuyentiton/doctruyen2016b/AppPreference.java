package com.tuyentiton.doctruyen2016b;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.tuyentiton.doctruyen2016b.model.Chapter;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Tuyen Ti Ton on 5/8/2017.
 */

public class AppPreference {
    private static final String LIST_CHAPTER = "list_chapters";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String PATH = "path";
    private SharedPreferences preferences;
    private Gson mGson;
    private ArrayList<Chapter> chapters = new ArrayList<>();

    public void init(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        mGson = new GsonBuilder().create();
        readData();
    }
	
    private void readData() {
        String stringData = preferences.getString(LIST_CHAPTER, null);
        if (stringData != null) {
            Type type = new TypeToken<ArrayList<Chapter>>() {
            }.getType();
            chapters = mGson.fromJson(stringData, type);
        }

    }

    public ArrayList<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<Chapter> chapters) {
        if (this.chapters.size() != 0) {
            clearData();
        }
        this.chapters.addAll(chapters);
        preferences.edit().putString(LIST_CHAPTER,
                mGson.toJson(this.chapters)).commit();
    }

    public void clearData() {
        chapters.clear();
        preferences.edit().putString(LIST_CHAPTER, mGson.toJson(this.chapters)).commit();
    }

    public void setUser(String name, String email) {
        preferences.edit().putString(NAME, name).commit();
        preferences.edit().putString(EMAIL, email).commit();
    }

    public String getName() {
        return preferences.getString(NAME, "");
    }

    public String getEmail() {
        return preferences.getString(EMAIL, "");
    }

    public void setPathImage(String path){
        preferences.edit().putString(PATH, path).commit();
    }

    public String getPath(){
        return preferences.getString(PATH, null);
    }

}
