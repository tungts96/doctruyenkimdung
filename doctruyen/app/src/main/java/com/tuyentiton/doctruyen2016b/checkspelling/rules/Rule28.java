/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule28 extends Rule {

    String x = null;
    //Nguyên âm "ú", "ù", "ủ", "ụ" đi đằng sau sẽ là các nguyên âm "i", "a", "y", 
    String temp = "iay";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ú") || x1.equalsIgnoreCase("ù") || x1.equalsIgnoreCase("ủ") || x1.equalsIgnoreCase("ụ")) {
                String x2 = x.charAt(i + 1) + "";
                if (Constant.VOWEL.contains(x2)) {
                    if (!temp.contains(x2)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule28 Error -->" + x);
    }
}
