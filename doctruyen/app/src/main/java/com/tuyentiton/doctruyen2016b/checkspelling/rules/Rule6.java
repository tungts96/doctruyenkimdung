/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import android.util.Log;

import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule6 extends Rule {

    //Có một số phụ âm được phép đứng cuối nhưng nó phải đi kèm với một phụ âm khác. Hãy xác định các cặp này: nh, ch, ng, 
    String x = null;

    public boolean checkValid(String x) {
        this.x = x;
        if (x.length() >= 3) {
            String consonant = Constant.CONSONANT;
            String x1 = x.charAt(x.length() - 1) + "";
            String x2 = x.charAt(x.length() - 2) + "";
            if (consonant.contains(x1)) {
                if (consonant.contains(x2)) {
                    String result = x2 + x1;
                    if (!result.equalsIgnoreCase("nh")) {
                        if (!result.equalsIgnoreCase("ch")) {
                            if (!result.equalsIgnoreCase("ng")) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        Log.e("checkSpell","Rule6 Error -->" + x);
    }

}
