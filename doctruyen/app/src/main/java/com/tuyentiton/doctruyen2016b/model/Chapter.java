package com.tuyentiton.doctruyen2016b.model;

import java.io.Serializable;

/**
 * Created by Son tung on 3/14/2017.
 */

public class Chapter implements Serializable{
    private int idStory;
    private int idChapter;
    private String nameChapter;
    private String contentOfChapter;
    private String timeRead;

    public Chapter() {
    }

    public Chapter(int idChapter, int idStory, String nameChapter,String contentOfChapter) {
        this.idChapter = idChapter;
        this.idStory = idStory;
        this.nameChapter = nameChapter;
        this.contentOfChapter = contentOfChapter;
    }

    public String getContentOfChapter() {
        return contentOfChapter;
    }

    public void setContentOfChapter(String contentOfChapter) {
        this.contentOfChapter = contentOfChapter;
    }

    public int getIdChapter() {
        return idChapter;
    }

    public void setIdChapter(int idChapter) {
        this.idChapter = idChapter;
    }

    public int getIdStory() {
        return idStory;
    }

    public void setIdStory(int idStory) {
        this.idStory = idStory;
    }

    public String getNameChapter() {
        return nameChapter;
    }

    public void setNameChapter(String nameChapter) {
        this.nameChapter = nameChapter;
    }

    public String getTimeRead() {
        return timeRead;
    }

    public void setTimeRead(String timeRead) {
        this.timeRead = timeRead;
    }
}
