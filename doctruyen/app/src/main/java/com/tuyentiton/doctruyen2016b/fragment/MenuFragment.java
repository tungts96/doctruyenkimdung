package com.tuyentiton.doctruyen2016b.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.activities.SplashActivity;

import java.io.File;
import java.nio.ByteBuffer;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Tuyen Ti Ton on 2/16/2017.
 */

public class MenuFragment extends Fragment implements View.OnClickListener {

    //Ảnh đại diện
    private ImageView imgAvatar, imgCover, imgUpdateAvatar;
    private Uri uri;
    private final int CAMERA_CODE = 0;
    private final int GALLEY_CODE = 1;
    private final int CROP_CODE = 2;
    private final String KEY_AVATAR = "Avatar";
    private final String KEY_COVER = "Cover";
    public final static String KEY_NAME = "Name";
    public final static String KEY_EMAIL = "Email";
    private TextView tvNameAccount, tvEmailAccount;

    public static final int MENU_HOME = 0;
    public static final int MENU_FAVORITE = 1;
    public static final int MENU_SETTING = 2;
    public static final int MENU_INTRO = 3;
    public static final int MENU_LOGIN = 4;
    private Position position;

    RelativeLayout naviHome, naviLike, naviIntro, naviSetting, naviLogin;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu, container, false);
        naviHome = (RelativeLayout) rootView.findViewById(R.id.naviHome);
        naviHome.setOnClickListener(this);
        naviLike = (RelativeLayout) rootView.findViewById(R.id.naviLike);
        naviLike.setOnClickListener(this);
        naviIntro = (RelativeLayout) rootView.findViewById(R.id.naviIntro);
        naviIntro.setOnClickListener(this);
        naviSetting = (RelativeLayout) rootView.findViewById(R.id.naviSetting);
        naviSetting.setOnClickListener(this);
        naviLogin = (RelativeLayout) rootView.findViewById(R.id.naviLogin);
        naviLogin.setOnClickListener(this);
        imgAvatar = (ImageView) rootView.findViewById(R.id.imgAvatar);
        imgAvatar.setOnClickListener(this);
        imgCover = (ImageView) rootView.findViewById(R.id.imgCover);
        imgUpdateAvatar = (ImageView) rootView.findViewById(R.id.imgUpdateImage);
        imgUpdateAvatar.setOnClickListener(this);
        tvNameAccount = (TextView) rootView.findViewById(R.id.tvUserName);
        tvEmailAccount = (TextView) rootView.findViewById(R.id.tvEmailOfUserName);
        String nameAccount = getFromSharePreference(KEY_NAME);
        String emailAccount = getFromSharePreference(KEY_EMAIL);
        if (nameAccount != null && emailAccount != null) {
            tvNameAccount.setText(nameAccount);
            tvEmailAccount.setText(emailAccount);
        } else {
            tvNameAccount.setText("Name Account");
            tvEmailAccount.setText("emailofaccount@gmail.com");
        }
//        String avatar = getFromSharePreference(KEY_AVATAR);
//        if (avatar != null){
//            Bitmap bitmap = StringToBitMap(avatar);
//            imgAvatar.setImageBitmap(bitmap);
//        }
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            position = (Position) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        position = null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.naviHome:
                setChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviLogin);
                setNoChooseBackgroundForMenu(naviSetting);
                position.posionForNavi(MENU_HOME);
                break;
            case R.id.naviLike:
                setChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviLogin);
                position.posionForNavi(MENU_FAVORITE);
                break;
            case R.id.naviIntro:
                setChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviLogin);
                position.posionForNavi(MENU_INTRO);
                break;
            case R.id.naviSetting:
                setChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviHome);
                setNoChooseBackgroundForMenu(naviLogin);
                position.posionForNavi(MENU_SETTING);
                showDialogSetting();
                break;
            case R.id.naviLogin:
                setChooseBackgroundForMenu(naviLogin);
                setNoChooseBackgroundForMenu(naviLike);
                setNoChooseBackgroundForMenu(naviIntro);
                setNoChooseBackgroundForMenu(naviSetting);
                setNoChooseBackgroundForMenu(naviHome);
                position.posionForNavi(MENU_LOGIN);
                break;
            case R.id.imgAvatar:
                checkPermission();
                menuUpdateAvatar();
                break;
            case R.id.imgUpdateImage:
                checkPermission();
                menuUpdateAvatar();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 111) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                menuUpdateAvatar();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_CODE:
                if (resultCode == RESULT_OK) {
                    cropImage();
                }
                break;
            case GALLEY_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    uri = data.getData();
                    cropImage();
                }
                break;
            case CROP_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    Bundle bundle = data.getExtras();
                    Bitmap bitmap = bundle.getParcelable("data");
                    imgAvatar.setImageBitmap(bitmap);
                    imgCover.setImageBitmap(bitmap);
                    //saveToSharePreference(KEY_AVATAR,BitMapToString(bitmap));
                }
                break;
        }
    }

    public String BitMapToString(Bitmap bitmap) {
//        ByteArrayOutputStream baos = new  ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
//        byte [] b=baos.toByteArray();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int size = bitmap.getRowBytes() * bitmap.getHeight();
        ByteBuffer byteBuffer = ByteBuffer.allocate(size);
        bitmap.copyPixelsToBuffer(byteBuffer);
        byte[] byteArray = byteBuffer.array();
        String temp = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return temp;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    //lưu lại preference
    private void saveToSharePreference(String values, String key) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, values);
        editor.commit();
    }

    //lấy ra từ preference
    private String getFromSharePreference(String key) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        String s = sharedPreferences.getString(key, "");
        return s;
    }

    //menu update Avatar
    private void menuUpdateAvatar() {
        PopupMenu menu = new PopupMenu(getContext(), imgUpdateAvatar);
        menu.inflate(R.menu.menu_update_image);
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.mnuFromCamera) {
                    getImageFromCamera();
                } else if (item.getItemId() == R.id.mnuFromGalley) {
                    getImageFromGalley();
                }
                return false;
            }
        });
        menu.show();
    }

    //Intent Crop ảnh
    private void cropImage() {
        Log.d("__", "yep");
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(uri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("outputX", 180);
            cropIntent.putExtra("outputY", 180);
            cropIntent.putExtra("aspectX", 3);
            cropIntent.putExtra("aspectY", 4);
            cropIntent.putExtra("scaleUpIfNeeded", true);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, CROP_CODE);
        } catch (Exception e) {

        }
    }

    //Intent lấy ảnh từ thư viện ảnh
    private void getImageFromGalley() {

        Intent galleyIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(galleyIntent, "Select Image From Gallery"), GALLEY_CODE);
    }

    //Intent lấy ảnh từ Camera
    private void getImageFromCamera() {
        Intent camIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "file" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        uri = Uri.fromFile(file);
        camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        camIntent.putExtra("return-data", true);
        startActivityForResult(camIntent, CAMERA_CODE);
    }

    private void showDialogSetting() {
        String name = getFromSharePreference(KEY_NAME);
        String email = getFromSharePreference(KEY_EMAIL);
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customer_dialog_setting);
        dialog.show();
        final EditText edtNameAccount = (EditText) dialog.findViewById(R.id.edtNameAccount);
        edtNameAccount.setText(name);
        final EditText edtEmailAccount = (EditText) dialog.findViewById(R.id.edtEmailAccount);
        edtEmailAccount.setText(email);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCanelDialog);
        btnOk.setOnClickListener(new View.OnClickListener() {
            String finalName;
            String finalEmail;

            @Override
            public void onClick(View v) {
                if (edtNameAccount.getText().toString().isEmpty()) {
                    edtNameAccount.setError("Chưa điền tên");
                    edtNameAccount.requestFocus();
                    return;
                } else {
                    if (edtEmailAccount.getText().toString().isEmpty()) {
                        edtEmailAccount.setError("Chưa điền email");
                        edtEmailAccount.requestFocus();
                        return;
                    } else {
                        finalName = edtNameAccount.getText().toString();
                        finalEmail = edtEmailAccount.getText().toString();
                    }
                }
                tvNameAccount.setText(finalName);
                tvEmailAccount.setText(finalEmail);
                saveToSharePreference(finalName, KEY_NAME);
                saveToSharePreference(finalEmail, KEY_EMAIL);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setChooseBackgroundForMenu(RelativeLayout relativeLayout) {
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.menuNavi));
    }

    private void setNoChooseBackgroundForMenu(RelativeLayout relativeLayout) {
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.frmMenu));
    }

    private void showToast(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    public interface Position {
        void posionForNavi(int position);
    }

    void checkPermission() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    111);
            return;
        }
    }

}
