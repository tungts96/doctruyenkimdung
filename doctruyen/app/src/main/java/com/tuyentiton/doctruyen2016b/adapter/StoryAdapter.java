package com.tuyentiton.doctruyen2016b.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.activities.ReadActivity;
import com.tuyentiton.doctruyen2016b.database.KimDungSQLite;
import com.tuyentiton.doctruyen2016b.dialog.NoticeDialog;
import com.tuyentiton.doctruyen2016b.fragment.MainFragment;
import com.tuyentiton.doctruyen2016b.model.Story;

import java.util.ArrayList;
import java.util.List;

import static com.tuyentiton.doctruyen2016b.fragment.MainFragment.ADAPTER_LIST_LOVE_STORY;
import static com.tuyentiton.doctruyen2016b.fragment.MainFragment.ADAPTER_LIST_STORY;

/**
 * Created by Son tung on 2/28/2017.
 */

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.StoryViewHolder> {

    private String type;
    private List<Story> listStory;
    private Context context;
    private LayoutInflater mLayoutInflater;
    SQLiteDatabase database = null;
    MainFragment fragment;
    private int viewType;

    public StoryAdapter(Context context, List<Story> listStory, String type, int viewType) {
        this.context = context;
        this.listStory = listStory;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.type = type;
        this.viewType = viewType;
    }

    public void updateData(ArrayList<Story> list) {
        listStory.clear();
        listStory.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public StoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemStory;
        if (this.viewType == 0) {
            itemStory = mLayoutInflater.inflate(R.layout.item_stories, null);
        } else {
            itemStory = mLayoutInflater.inflate(R.layout.item_strories2, null);
        }
        return new StoryViewHolder(itemStory);
    }

    @Override
    public void onBindViewHolder(final StoryViewHolder holder, final int position) {
        final Story story = listStory.get(position);
        fragment = new MainFragment(context);
        if (viewType == 0) {
            holder.tvNameStory.setText(story.getName());
            Picasso.with(context)
                    .load(Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + story.getImage()))
                    .fit()
                    .centerInside()
                    .into(holder.imgStory);
            holder.imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final PopupMenu popupMenu = new PopupMenu(context, holder.imgMore);
                    popupMenu.inflate(R.menu.menu_more);
                    if (story.isLike()) {
                        MenuItem item1 = popupMenu.getMenu().findItem(R.id.mnuLike);
                        item1.setTitle("Xóa khỏi danh sách yêu thích");
                    }
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.mnuLike:
                                    if (type.equals(ADAPTER_LIST_STORY)) {
                                        changeLike(story);
                                        fragment.update(story, ADAPTER_LIST_STORY);
                                    } else if (type.equals(ADAPTER_LIST_LOVE_STORY)) {
                                        changeLike(story);
                                        listStory.remove(position);
                                        fragment.update(story, ADAPTER_LIST_LOVE_STORY);
                                    }
                                    break;
                                case R.id.mnuRead:
                                    changeActivity(position);
                                    break;
                                case R.id.mnuDescribe:
                                    showDescribe(story);
                                    break;
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });
        } else {
            if (story.isLike()) {
                holder.tvLike.setText("Bỏ thích");
            } else {
                holder.tvLike.setText("Thích");
            }
            Picasso.with(context)
                    .load(Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + story.getImage()))
                    .fit()
                    .centerInside()
                    .into(holder.imgStory2);
            holder.imgStory2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeActivity(position);
                }
            });
            holder.tvTitle2.setText("Truyện: " + story.getName());
            holder.tvStatus.setText("Trạng thái: Full ");
            holder.tvRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeActivity(position);
                }
            });
            holder.tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeLike(story);
                    fragment.update(story, ADAPTER_LIST_STORY);
                }
            });
            holder.tvDescrition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDescribe(story);
                }
            });
            holder.tvTitle2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeActivity(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listStory.size();
    }

    private void showDescribe(Story story) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.border));
        dialog.setContentView(R.layout.customer_dialog_describestory);
        TextView tvNameStory = (TextView) dialog.findViewById(R.id.tvNameOfStory);
        ImageView imgStory = (ImageView) dialog.findViewById(R.id.imgBgStory);
        ImageView imgCancel = (ImageView) dialog.findViewById(R.id.imgCancelDialog);
        TextView tvContentDescribe = (TextView) dialog.findViewById(R.id.tvContentDescription);
        tvNameStory.setText(story.getName());
        Picasso.with(context)
                .load(Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + story.getImage()))
                .fit()
                .centerInside()
                .into(imgStory);
        tvContentDescribe.setText("\t \t " + story.getDescribe());
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void changeLike(Story story) {
        if (story.isLike()) {
            story.setLike(false);
            setLikeForStory(story, 0);
//            NoticeDialog dialog = new NoticeDialog("Thông Báo", "Đã xóa khỏi danh sách yêu thích", context);
//            dialog.showDialog();
            Toast.makeText(context, "Đã xóa khỏi danh sách yêu thích", Toast.LENGTH_SHORT).show();
        } else {
            story.setLike(true);
            setLikeForStory(story, 1);
//            NoticeDialog dialog = new NoticeDialog("Thông Báo", "Đã thêm vào danh sách yêu thích", context);
//            dialog.showDialog();
            Toast.makeText(context, "Đã thêm vào danh sách yêu thích", Toast.LENGTH_SHORT).show();
        }
    }

    private void setLikeForStory(Story story, int i) {
        database = KimDungSQLite.initDatabase(context);
        database.execSQL("UPDATE " + KimDungSQLite.DATABASE_TABLE_KIMDUNG + " SET auID=" + i + " WHERE stID=" + story.getIdStory());
    }

    private void changeActivity(int position) {
        Intent i = new Intent(context, ReadActivity.class);
        Story story = listStory.get(position);
        i.putExtra("STORY", story);
        context.startActivity(i);
    }

    class StoryViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgStory, imgMore;
        private TextView tvNameStory;

        private ImageView imgStory2;
        private TextView tvTitle2, tvStatus, tvRead, tvLike, tvDescrition;

        public StoryViewHolder(View itemView) {
            super(itemView);
            if (viewType == 0) {
                imgStory = (ImageView) itemView.findViewById(R.id.imgStory);
                imgMore = (ImageView) itemView.findViewById(R.id.imgMore);
                tvNameStory = (TextView) itemView.findViewById(R.id.tvNameStory);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        changeActivity(getAdapterPosition());
                    }
                });
            } else {
                imgStory2 = (ImageView) itemView.findViewById(R.id.imgStory2);
                tvDescrition = (TextView) itemView.findViewById(R.id.tvDescription);
                tvTitle2 = (TextView) itemView.findViewById(R.id.tvTitle2);
                tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
                tvRead = (TextView) itemView.findViewById(R.id.tvRead);
                tvLike = (TextView) itemView.findViewById(R.id.tvLike);
            }
        }
    }
}
