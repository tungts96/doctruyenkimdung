package com.tuyentiton.doctruyen2016b.fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.activities.ReadActivity;
import com.tuyentiton.doctruyen2016b.adapter.ChapterAdater;
import com.tuyentiton.doctruyen2016b.checkspelling.ConvertUnsigned;
import com.tuyentiton.doctruyen2016b.database.KimDungSQLite;
import com.tuyentiton.doctruyen2016b.interfaces.RecyclerviewItemClick;
import com.tuyentiton.doctruyen2016b.model.Chapter;
import com.tuyentiton.doctruyen2016b.model.Story;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.tuyentiton.doctruyen2016b.database.KimDungSQLite.DATABASE_TABLE_STKIMDUNG;

/**
 * Created by Tuyen Ti Ton on 3/14/2017.
 */

public class ChapterFragment extends Fragment implements SearchView.OnQueryTextListener, View.OnClickListener {

    ReadActivity readActivity;
    SQLiteDatabase database = null;
    private RecyclerView rcvChapter;
    private ArrayList<Chapter> arrChapter, arrCoppyChapter;
    private ChapterAdater chapterAdater;
    private Story story;
    private Toolbar actionBarRead;
    private SearchView searchView;
    private ImageView imgBack;
    private TextView tvNameStory;
    View root;

    private ConvertUnsigned convertUnsigned;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_chapter, container, false);
        convertUnsigned = new ConvertUnsigned();
        getStoryFromActivity();
        addActionBar();
        imgBack = (ImageView) root.findViewById(R.id.imgBack);
        searchView = (SearchView) root.findViewById(R.id.imgSearch);
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        tvNameStory = (TextView) root.findViewById(R.id.tvTitleNameStory);
        tvNameStory.setText(story.getName());
        addEvents();
        return root;
    }

    private void addEvents() {
        imgBack.setOnClickListener(this);
        searchView.setOnQueryTextListener(this);
        //final LinearLayoutManager layoutManager = (LinearLayoutManager) rcvChapter.getLayoutManager();
//        rcvChapter.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                totalItemCount = layoutManager.getItemCount();
//                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
//                if (totalItemCount <= (lastVisibleItem + visibleThresold)){
//                    loadMoreChapterOfStory(arrChapter.get(totalItemCount-1).getIdChapter(),story);
//                    Log.e("ST RCV",arrChapter.size()+"");
//                }
//            }
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//        });
    }

    private void addActionBar() {
        actionBarRead = (Toolbar) root.findViewById(R.id.customActionBarRead);
        readActivity.getSupportActionBar();
        readActivity.setSupportActionBar(actionBarRead);
    }

    private void getStoryFromActivity() {
        readActivity = (ReadActivity) getActivity();
        story = readActivity.getStory();
        initRecyclerChapter(story);
    }

    private void initRecyclerChapter(final Story story) {
        rcvChapter = (RecyclerView) root.findViewById(R.id.rcvChapter);
        arrChapter = getListChapterOfStory(story);
        arrCoppyChapter = new ArrayList<>();
        arrCoppyChapter.addAll(arrChapter);
        chapterAdater = new ChapterAdater(getContext(), arrChapter);
        chapterAdater.setRes(Uri.parse("android.resource://" + getContext().getPackageName() + "/drawable/" + story.getImage()));
        chapterAdater.setRecyclerviewItemClick(new RecyclerviewItemClick() {
            @Override
            public void itemClick(int position) {
                hideKeyboard();
                Bundle args = new Bundle();
                args.putString("NAMESTORY",story.getName());
                args.putSerializable("CHAPTER", arrChapter.get(position));
                args.putSerializable("ARRCHAPTER", arrChapter);
                Fragment fragment = new ReadChapterFragment();
                fragment.setArguments(args);
                readActivity.addFragmentContent(fragment);
                arrChapter.get(position).setTimeRead(getRealTime());
                chapterAdater.notifyDataSetChanged();
                setTimeReadForChapter(arrChapter.get(position));
            }
        });
        rcvChapter.setAdapter(chapterAdater);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcvChapter.setLayoutManager(linearLayoutManager);
    }

//    //get 9 chapter
//    public ArrayList<Chapter> getListFirstChapter(Story story){
//        int i = 0;
//        ArrayList<Chapter> listChapterOfStory = new ArrayList<>();
//        listChapterOfStory.clear();
//        database = KimDungSQLite.initDatabase(getContext());
//        Cursor c = database.rawQuery("Select * from " + DATABASE_TABLE_STKIMDUNG + " where stID=" + story.getIdStory(), null);
//        while (c.moveToNext() && i<visibleThresold) {
//            int idChapter = c.getInt(0);
//            int idStory = story.getIdStory();
//            String nameChapter = c.getString(1);
//            String contentOfChapter = c.getString(3);
//            contentOfChapter = contentOfChapter.replaceAll("<br/>", "\n");
//            contentOfChapter = contentOfChapter.replaceAll("<p>", "");
//            contentOfChapter = contentOfChapter.replaceAll("<br />", "\n");
//            Chapter chapter = new Chapter(idChapter, idStory, nameChapter, contentOfChapter);
//            chapter.setTimeRead(c.getString(5));
//            listChapterOfStory.add(chapter);
//            i++;
//        }
//        c.close();
//        return listChapterOfStory;
//    }
//
//    //load more chapter
//    public void loadMoreChapterOfStory(int id,Story story) {
//        int i = 0;
//        ArrayList<Chapter> listChapterOfStory = new ArrayList<>();
//        listChapterOfStory.clear();
//        database = KimDungSQLite.initDatabase(getContext());
//        Cursor c = database.rawQuery("Select * from " + DATABASE_TABLE_STKIMDUNG + " where (stID=" + story.getIdStory() + " and deID>"+id+")", null);
//        while (c.moveToNext() && i<visibleThresold) {
//            int idChapter = c.getInt(0);
//            int idStory = story.getIdStory();
//            String nameChapter = c.getString(1);
//            String contentOfChapter = c.getString(3);
//            contentOfChapter = contentOfChapter.replaceAll("<br/>", "\n");
//            contentOfChapter = contentOfChapter.replaceAll("<p>", "");
//            contentOfChapter = contentOfChapter.replaceAll("<br />", "\n");
//            Chapter chapter = new Chapter(idChapter, idStory, nameChapter, contentOfChapter);
//            chapter.setTimeRead(c.getString(5));
//            listChapterOfStory.add(chapter);
//            i++;
//        }
//        c.close();
//        if (listChapterOfStory.size() != 0){
//            arrChapter.addAll(listChapterOfStory);
//            arrCoppyChapter.addAll(listChapterOfStory);
//            chapterAdater.notifyDataSetChanged();
//        }
//    }

    //get list chap ter
    public ArrayList<Chapter> getListChapterOfStory(Story story) {
        ArrayList<Chapter> listChapterOfStory = new ArrayList<>();
        listChapterOfStory.clear();
        database = KimDungSQLite.initDatabase(getContext());
        Cursor c = database.rawQuery("Select * from " + DATABASE_TABLE_STKIMDUNG + " where stID=" + story.getIdStory(), null);
        while (c.moveToNext()) {
            int idChapter = c.getInt(0);
            int idStory = story.getIdStory();
            String nameChapter = c.getString(1);
            String contentOfChapter = c.getString(3);
            contentOfChapter = contentOfChapter.replaceAll("<br/>", "\n");
            contentOfChapter = contentOfChapter.replaceAll("<p>", "");
            contentOfChapter = contentOfChapter.replaceAll("<br />", "\n");
            Chapter chapter = new Chapter(idChapter, idStory, nameChapter, contentOfChapter);
            chapter.setTimeRead(c.getString(5));
            listChapterOfStory.add(chapter);
        }
        c.close();
        return listChapterOfStory;
    }

    public void setTimeReadForChapter(Chapter chapter) {
        String time = getRealTime();
        database = KimDungSQLite.initDatabase(getContext());
        database.execSQL("UPDATE " + KimDungSQLite.DATABASE_TABLE_STKIMDUNG + " SET deDate=" + "\"" + time + "\"" + " WHERE deID=" + chapter.getIdChapter());
    }

    private String getRealTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(Calendar.getInstance().getTime());
        return date;
    }

    private void filter(String text) {
        arrChapter.clear();
        if (text.isEmpty()) {
            arrChapter.addAll(arrCoppyChapter);
        } else {
            //text = text.toLowerCase();
            for (Chapter item : arrCoppyChapter) {
                String nameChapter = convertUnsigned.convertString(item.getNameChapter());
                if (nameChapter.contains(text)) {
                    arrChapter.add(item);
                }
            }
        }
        chapterAdater.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        filter(convertUnsigned.convertString(query));
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter(convertUnsigned.convertString(newText));
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBack) {
//            Intent i = new Intent(getActivity(),MainActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(i);
            Animation animFade = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            imgBack.startAnimation(animFade);
            getActivity().onBackPressed();
        }
    }

//    private void showDialogSearch() {
//        final Dialog dialog = new Dialog(getContext());
//        dialog.setCancelable(false);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.customer_dialog_search);
//        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
//        dialog.show();
//        EditText edtNameStory = (EditText) dialog.findViewById(R.id.edtNameStory);
//        edtNameStory.setText(story.getName());
//        edtNameStory.setEnabled(false);
//        final EditText edtQueryText = (EditText) dialog.findViewById(R.id.edtQueryText);
//        TextView btnSearchText = (TextView) dialog.findViewById(R.id.btnSearch);
//        RelativeLayout btnSearchVoice = (RelativeLayout) dialog.findViewById(R.id.searchVoice);
//        TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
//        btnSearchText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String queryText = edtQueryText.getText().toString();
//                if (queryText.length() > 0){
//                    dialog.dismiss();
//                }
//            }
//        });
//        btnSearchVoice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//    }

//    private void filterString(String query){
//        arrChapter.clear();
//        if (query.isEmpty()) {
//            arrChapter.addAll(arrCoppyChapter);
//        } else {
//            SearchStringWithRegex regex = new SearchStringWithRegex();
//            regex.setStringQuery(query);
//            //text = text.toLowerCase();
//            for (Chapter item : arrCoppyChapter) {
//                String name = convertUnsigned.convertString(item.getContentOfChapter());
//                if (regex.checkString(name)){
//                    arrChapter.add(item);
//                };
//            }
//        }
//        if (arrChapter.size() ==0){
//            Toast.makeText(readActivity, "asdsa", Toast.LENGTH_SHORT).show();
//        } else {
//            chapterAdater.notifyDataSetChanged();
//        }
//    }
//
//    private static ProgressDialog dialog;
//
//    public static void showProgressDialog(Context nContext) {
//        try {
//            hideProgressDialog();
//            dialog = new ProgressDialog(nContext);
//            dialog.setCancelable(true);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog.getWindow().setDimAmount(0f);
//
//            dialog.show();
//            dialog.setContentView(R.layout.testprogressbar);
//        } catch (Exception e) {
//        }
//    }
//
//    public static void hideProgressDialog() {
//        try {
//            if (dialog != null && dialog.isShowing())
//                dialog.dismiss();
//        } catch (Exception e) {
//
//        }
//    }

    protected void hideKeyboard() {
        if (getActivity() == null) {
            return;
        }
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
