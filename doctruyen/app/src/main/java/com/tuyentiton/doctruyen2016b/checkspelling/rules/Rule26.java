/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule26 extends Rule {

    String x = null;
    //Nguyên âm "ỡ" nếu đi sau nó là "i" thì từ đó phải là "hỡi"

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("ỡ")) {
                String x2 = x.charAt(i + 1) + "";
                if (x2.equalsIgnoreCase("i")) {
                    if (!x.equalsIgnoreCase("hỡi") && !x.equalsIgnoreCase("cưỡi") && !x.equalsIgnoreCase("lưỡi")) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void show() {
        System.out.println("Rule26 error -->" + x);
    }
}
