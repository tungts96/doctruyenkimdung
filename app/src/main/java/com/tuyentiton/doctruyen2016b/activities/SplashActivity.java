package com.tuyentiton.doctruyen2016b.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tuyentiton.doctruyen2016b.R;

public class SplashActivity extends AppCompatActivity {

    private TextView tvKiem, tvHiep, tvKim, tvDung;
    private Animation zoomOut, qrcodeClose;
    private LinearLayout layoutTitle;
    private ImageView imgQrcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tvHiep = (TextView) findViewById(R.id.tvHiep);
        tvKiem = (TextView) findViewById(R.id.tvKiem);
        tvKim = (TextView) findViewById(R.id.tvKim);
        tvDung = (TextView) findViewById(R.id.tvDung);
        layoutTitle = (LinearLayout) findViewById(R.id.layoutTitle);
        imgQrcode = (ImageView) findViewById(R.id.imgQrcode);
        Typeface thuPhap = Typeface.createFromAsset(getAssets(), "font/Thuphap");
        tvHiep.setTypeface(thuPhap);
        tvKim.setTypeface(thuPhap);
        tvKiem.setTypeface(thuPhap);
        tvDung.setTypeface(thuPhap);
        zoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        qrcodeClose = AnimationUtils.loadAnimation(this, R.anim.qrcode_close);
        layoutTitle.startAnimation(zoomOut);
        zoomOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                layoutTitle.clearAnimation();
                imgQrcode.setVisibility(View.VISIBLE);
                imgQrcode.startAnimation(qrcodeClose);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        qrcodeClose.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgQrcode.clearAnimation();
                final Handler timerHandler = new Handler();
                final Runnable timerRunnable = new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                };
                timerHandler.postDelayed(timerRunnable, 500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

}
