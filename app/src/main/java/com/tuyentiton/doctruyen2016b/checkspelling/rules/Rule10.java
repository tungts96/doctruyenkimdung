/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule10 extends Rule {

    String x = null;
    //Chỉ có một số nguyên âm được phép đứng đằng sau "á" để tạo thành cặp nguyên âm. Hãy liệt kê: ái, áu, áo, áy
    String temp = "aeêăâơôư";

    public boolean checkValid(String x) {
        this.x = x;
        for (int i = 0; i < x.length() - 1; i++) {
            String x1 = x.charAt(i) + "";
            if (x1.equalsIgnoreCase("á")) {
                String x2 = x.charAt(i + 1) + "";
                if (temp.contains(x2)) {
                    return false;
                }
            }
        }
        return true;
    }

    public String show(){
        return "Rule10 Error -->" + x;
    }
}
