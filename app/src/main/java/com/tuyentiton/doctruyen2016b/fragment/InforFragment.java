package com.tuyentiton.doctruyen2016b.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuyentiton.doctruyen2016b.AppPreference;
import com.tuyentiton.doctruyen2016b.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Son tung on 4/30/2017.
 */

public class InforFragment extends Fragment implements View.OnClickListener {

    RelativeLayout rlVoteApp, rlShareApp, rlMailApp;
    TextView tvNameAccount, tvEmailAccount;
    private AppPreference appPreference;

    public InforFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_infor_acount, container, false);
        appPreference = new AppPreference();
        appPreference.init(getContext());
        rlVoteApp = (RelativeLayout) root.findViewById(R.id.rlVoteApp);
        rlVoteApp.setOnClickListener(this);
        rlShareApp = (RelativeLayout) root.findViewById(R.id.rlShareApp);
        rlShareApp.setOnClickListener(this);
        rlMailApp = (RelativeLayout) root.findViewById(R.id.rlMailApp);
        rlMailApp.setOnClickListener(this);
        tvNameAccount = (TextView) root.findViewById(R.id.tvNameAccount);
        tvEmailAccount = (TextView) root.findViewById(R.id.tvEmail);
        tvNameAccount.setText("Tên người dùng: " + appPreference.getName());
        tvEmailAccount.setText("Email: " + appPreference.getEmail());
        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlVoteApp:
                rateApp();
                break;
            case R.id.rlShareApp:
                shareApp();
                break;
            case R.id.rlMailApp:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"tungbxars@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Lỗi chính tả");
                intent.putExtra(Intent.EXTRA_TEXT, "Nhập góp ý ở đây!");
                intent.setType("message/rfc822");
                startActivity(Intent.createChooser(intent, "Send Email"));
                break;
        }
    }

    private void shareApp() {
        final String appPackageName = getActivity().getPackageName();
        String myUrl = "https://play.google.com/store/apps/details?id=" + appPackageName;

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, myUrl);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share using..."));
    }

    public void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
        }
    }

    private String getFromSharePreference(String key) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(key, MODE_PRIVATE);
        String s = sharedPreferences.getString(key, "");
        return s;
    }

}

