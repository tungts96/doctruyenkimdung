package com.tuyentiton.doctruyen2016b.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.tuyentiton.doctruyen2016b.R;
import com.tuyentiton.doctruyen2016b.fragment.InforFragment;
import com.tuyentiton.doctruyen2016b.fragment.MainFragment;
import com.tuyentiton.doctruyen2016b.fragment.MenuFragment;
import com.tuyentiton.doctruyen2016b.ui.CustomActionBar;

import static com.tuyentiton.doctruyen2016b.fragment.MenuFragment.MENU_FAVORITE;
import static com.tuyentiton.doctruyen2016b.fragment.MenuFragment.MENU_HOME;
import static com.tuyentiton.doctruyen2016b.fragment.MenuFragment.MENU_INTRO;
import static com.tuyentiton.doctruyen2016b.fragment.MenuFragment.MENU_LOGIN;

public class MainActivity extends AppCompatActivity implements MenuFragment.Position {

    private CustomActionBar actionBar;
    private DrawerLayout drawerLayout;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private static  ViewPager viewPager;
    private static TabLayout tabLayout;

    public static int TAB_SELECTED = 0;
    private int countExit = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        actionBar = (CustomActionBar)findViewById(R.id.customActionBar);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        initView();

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        TAB_SELECTED = 0;
                        break;
                    case 1:
                        TAB_SELECTED = 1;
                        break;
                    case 2:
                        TAB_SELECTED = 2;
                        break;
                    default: TAB_SELECTED = 0;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setUpTabIcon();
    }

    public static void updateView() {
        viewPager.getAdapter().notifyDataSetChanged();
        setUpTabIcon();
    }

    private static void setUpTabIcon(){
        int[] tabIcons = {
                R.drawable.ic_home_white,
                R.drawable.ic_like_white,
                R.drawable.ic_account_white
        };
        for (int i=0;i<3;i++){
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
    }

    private void initView(){
        //actionBar.setShowType(CustomActionBar.SHOW_TITLE);
        actionBar.setTvTitle("Truyện Kim Dung");
        actionBar.setNaviListener(new CustomActionBar.NaviListener() {
            @Override
            public void onRightClick() {

            }

            @Override
            public void onLeftClick() {
                openDrawer();
            }
        });
    }

    private void closeDrawer(){
        if(drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void openDrawer(){
        hideKeyboard();
        if(!drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            countExit++;
//            if (countExit == 1){
//                showToast("Nhấn thêm một lần nữa để thoát");
//            } else {
//                finish();
//            }
//            return true;
//        }
//        return false;
//    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Nhấn thêm một lần nữa để thoát", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public void posionForNavi(int position) {
        switch (position){
            case MENU_HOME:
                viewPager.setCurrentItem(0);
                closeDrawer();
                break;
            case MENU_FAVORITE:
                viewPager.setCurrentItem(1);
                closeDrawer();
                break;
            case MENU_INTRO:
                viewPager.setCurrentItem(2);
                closeDrawer();
                break;
//            case  MENU_SETTING:
//                closeDrawer();
//                showToast("Setting");
//                break;
            case MENU_LOGIN:
                closeDrawer();
                finish();
                break;
        }
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideKeyboard() {
        if (this == null) {
            return;
        }
        View view = this.getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0 || position == 1){
                return MainFragment.newInstance(position);
            } else {
                return new InforFragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

    }

}
