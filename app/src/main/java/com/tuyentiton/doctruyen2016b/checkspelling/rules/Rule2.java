/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuyentiton.doctruyen2016b.checkspelling.rules;


import com.tuyentiton.doctruyen2016b.checkspelling.constant.Constant;

/**
 *
 * @author Tuyen Ti Ton
 */
public class Rule2 extends Rule {
    String x = null;
    // Một từ tiếng Việt có tối đa 5 phụ âm: nghiêng
    public boolean checkValid(String x) {
        this.x = x;
        int count = 0;
        String consonant = Constant.CONSONANT;
        for (int i = 0; i < x.length(); i++) {
            if (consonant.contains(x.charAt(i) + "")) {
                count++;
            }
        }
        if (count > 5) {
            return false;
        }
        return true;
    }

    public String show(){
        return "Rule2 Error -->" + x;
    }
}
